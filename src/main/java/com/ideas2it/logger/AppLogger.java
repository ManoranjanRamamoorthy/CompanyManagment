package com.ideas2it.logger;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * AppLogger acts as logger for the project CompanyManagement
 * </p>
 */
public class AppLogger {
    
    public AppLogger(String className) {
        logger = Logger.getLogger(className);
    }

    private static Logger logger;


    /**
     * <p>
     * Configures the logger using the .xml file included.
     * </p>
     */
    public static void configureLogger() {
        DOMConfigurator.configure(Constants.CONFIG_LOG);
    }

    /**
     * <p>
     * Logs the debug message in the log file
     * </p>
     */
    public static void logDebug(Exception exception) {
        logger.debug(exception.getMessage(), exception);
    }


    /**
     * <p>
     * Logs the error message in the log file.
     * </p>
     */
    public static void logError(Exception exception) {
        logger.error(exception.getMessage(), exception);
    }

    /**
     * <p>
     * Logs the warn message in the log file.
     * </p>
     */
    public static void logWarn(Exception exception) {
        logger.warn(exception.getMessage(), exception);
    }

    /**
     * <p>
     * Logs the fatal message in the log file.
     * </p>
     */
    public static void logFatal(Exception exception) {
        logger.fatal(exception.getMessage(), exception);
    }

    /**
     * <p>
     * Logs the info message in the log file.
     * </p>
     */
    public static void logInfo(String message) {
        logger.info(message);
    }
}
