package com.ideas2it.exception;

import java.lang.Exception;
import java.lang.Throwable;

/**
 * <p>
 * ApplicationException is a custom exception class that extends from Exception
 * </p>
 */
public class ApplicationException extends Exception {
    public ApplicationException(String errorMessage) {
        super(errorMessage);
    }

    public ApplicationException(Throwable exception) {
        super(exception);
    }
}
