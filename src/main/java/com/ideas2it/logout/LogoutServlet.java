package com.ideas2it.logout;

import java.io.PrintWriter;
import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogoutServlet extends HttpServlet {

    @RequestMapping("/LogoutServlet")
    public ModelAndView logout(HttpServletRequest request)
                                         throws ServletException, IOException {
        String forward = "/index.jsp";
        ModelAndView modelAndView = null;

        try {
            HttpSession session = request.getSession(Boolean.FALSE);
            session.removeAttribute("user");
            modelAndView = new ModelAndView(forward);
        } catch (Exception exception) {
            System.out.println(exception);
        } finally {
            return modelAndView;
        }
    }
}

