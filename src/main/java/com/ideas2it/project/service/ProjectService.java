package com.ideas2it.project.service;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;
/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of project details
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */
public interface ProjectService {

    /**
     * <p>
     * Gets the arguments and creates an object project that
     * is assigned to an array-list projects
     * </p>
     * @param    project
     *           the project object to be added.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    Project createProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Performs the deletion of an project
     * </p> 
     *
     * @param   id
     *          is the project Id of the project to be deleted
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean removeProject(int id) throws ApplicationException;

    /**
     * <p>
     * Updates title of project using project id as reference
     * </p>
     *
     * @param   project
     *          the project that is to be updated.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean modifyProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Getter for getting the project list
     * </p>
     *
     * @returns projects
     *          the list containing all projects
     */
    List<Project> retriveProjects() throws ApplicationException;

    /**
     * <P>
     * Returns the project with the given Id
     * </p>
     *
     * @param   projectId
     *          the id of the project
     * @returns project
     *          the project with the specified id
     */
    Project retriveProjectById(int projectId) throws ApplicationException;

    /**
     * <p>
     * Assigns the employees to a particular project
     * </p>
     *
     * @param   projectId
     *          the Id of the project to which the Employees are to be assigned
     * @param   employeeIds
     *          the Id's of the employees who are to be assigned to the project
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean assignEmployeesToProject(int projectId, List<Integer> employeeIds)
                                                    throws ApplicationException;

    /**
     * <p>
     * Removes an employee from the corresponding project
     * </p>
     *
     * @param   projectId
     *          the Id of the project to which the employees are to be assigned
     * @param   employeeId
     *          the Id of the employee who is to be removed
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean deassignEmployeeFromProject(int projectId, int employeeId)
                                                    throws ApplicationException;
    /**
     * <p>
     * Removes a project from a client using their Id's as references.
     * </p>
     *
     * @param   projectId
     *          the id of the project which is to be removed
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean removeProjectFromClient(int projectId) throws ApplicationException;

    /**
     * <p>
     * Assigns a project to the given client.
     * </p>
     *
     * @param   projectId
     *          the Id of the project which is to be assigned.
     * @param   client
     *          the client object to which the project is to be assigned.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean assignClientToProject(int projectId, Client client)
                                                    throws ApplicationException;
}
