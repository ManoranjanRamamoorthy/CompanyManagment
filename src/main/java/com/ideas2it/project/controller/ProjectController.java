package com.ideas2it.project.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.ProjectService;


/**
 * <p>
 * The class ProjectController stores, deletes, updates and displays project
 * details.
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R 
 */
@Controller
@RequestMapping("/ProjectController")
public class ProjectController extends HttpServlet {
    private ProjectService projectService;
    private ClientService clientService;
    private EmployeeService employeeService;
    private static final String UPDATE = "/jsp/editProject.jsp";
    private static final String DELETE_OR_DISPLAY =
                                                 "/jsp/displayAllProjects.jsp";
    private static final String INSERT = "/jsp/addProject.jsp";
    private static final String ASSIGN_EMPLOYEE = "/jsp/assignEmployee.jsp";
    private static final AppLogger logger =
                              new AppLogger(ProjectController.class.getName());

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    /**
     * <p>
     * deletes the Project object using the id.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_DELETE},
                                                    method = RequestMethod.POST)
    public ModelAndView deleteProject(
                @RequestParam(Constants.ID) int id) throws ServletException,
                                                          ApplicationException {
        ModelAndView modelAndView = null;
        try {
            projectService.removeProject(id);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECTS,
                                              projectService.retriveProjects());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Updates the Porject details as given by the user.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_EDIT},
                                                    method = RequestMethod.POST)
    public ModelAndView editProject(@RequestParam(Constants.Id) int id,
    @RequestParam(Constants.TITLE) String title,
    @RequestParam(Constants.DOMAIN) String domain,
    @RequestParam(Constants.DESCRIPTION) String description)
                                throws ServletException, ApplicationException {
        ModelAndView modelAndView = null;
        try {
            Project project = projectService.retriveProjectById(id);
            project.setTitle(title);
            project.setDomain(domain);
            project.setDescription(description);
            projectService.modifyProject(project);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECTS,
                                              projectService.retriveProjects());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Displays all the projects that are currently active.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_DISPLAY},
                                                     method = RequestMethod.GET)
    public ModelAndView displayProjects() throws ServletException,
            ApplicationException {
        ModelAndView modelAndView = null;
        try {
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECTS,
                                              projectService.retriveProjects());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Gets the details and adds them as a new project object
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_ADD},
                                                    method = RequestMethod.POST)
    public ModelAndView addProject(
            @ModelAttribute(Constants.PROJECT) Project project,
            @RequestParam(Constants.CLIENT_ID) int clientId)
            throws ServletException, ApplicationException {
        ModelAndView modelAndView = null;
        try {
            Client client = clientService.retriveClientById(clientId);
            client.getProjects().add(project);
            clientService.modifyClient(client);
            String forward =  DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECTS,
                                              projectService.retriveProjects());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * redirects the request to the respective insert project page by setting
     * necessary parameters
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_INSERT},
                                                    method = RequestMethod.POST)
    public ModelAndView redirectToAddProject(
                @RequestParam(Constants.CLIENT_ID) int clientId)
                                 throws ServletException, ApplicationException {
        ModelAndView modelAndView = null;
        try {
            String forward =  INSERT;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.CLIENT_ID, clientId);
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * redirects the request to the respective update project page by setting
     * necessary parameters
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_UPDATE},
                                                    method = RequestMethod.POST)
    public ModelAndView redirectToEditProject(
                                            @RequestParam(Constants.ID) int id)
                                 throws ServletException, ApplicationException {
        ModelAndView modelAndView = null;
        try {
            Project project = projectService.retriveProjectById(id);
            String forward =  UPDATE;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECT, project);
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Assigns Employees to the project by using the Employee id and
     * project Id as references.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_ASSIGN_EMPLOYEES},
                                                    method = RequestMethod.POST)
    public ModelAndView assignEmployeesToProject(
                            @RequestParam(Constants.EMPLOYEE_ID) int employeeId,
                            @RequestParam(Constants.ID) int id)
                                 throws ServletException, ApplicationException {
        ModelAndView modelAndView = null;
        try {
            List<Integer> employeeIds = new ArrayList<>();
            employeeIds.add(employeeId);
            projectService.assignEmployeesToProject(id, employeeIds);
            String forward =  ASSIGN_EMPLOYEE;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECT,
                                         projectService.retriveProjectById(id));
            modelAndView.addObject(Constants.EMPLOYEES,
                                  employeeService.retriveUnassignedEmployees());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * redirects the request to the respective update assign employee page by 
     * setting necessary parameters
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_GET_EMPLOYEE},
                                                    method = RequestMethod.POST)
    public ModelAndView redirectToAssignEmployees(
                                @RequestParam(Constants.ID) int id)
                                 throws ServletException, ApplicationException {
        ModelAndView modelAndView = null;
        try {
            String forward =  ASSIGN_EMPLOYEE;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECT,
                                         projectService.retriveProjectById(id));
            modelAndView.addObject(Constants.EMPLOYEES,
                                  employeeService.retriveUnassignedEmployees());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * De-assigns Employees from the project by using the Employee id and
     * project Id as references.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_DEASSIGN_EMPLOYEE},
                                                     method = RequestMethod.GET)
    public ModelAndView deAssignEmployee(@RequestParam(Constants.ID) int id,
    @RequestParam(Constants.EMPLOYEE_ID) int employeeId)
                                 throws ServletException, ApplicationException {
        ModelAndView modelAndView = null;
        try {
            projectService.deassignEmployeeFromProject(id, employeeId);
            String forward =  ASSIGN_EMPLOYEE;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.PROJECT,
                                         projectService.retriveProjectById(id));
            modelAndView.addObject(Constants.EMPLOYEES,
                                  employeeService.retriveUnassignedEmployees());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }
}
