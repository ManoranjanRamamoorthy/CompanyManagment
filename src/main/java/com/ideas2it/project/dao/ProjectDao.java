package com.ideas2it.project.dao;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * The interface ProjectDao performs storing, updating, deleting and retrieving
 * data in the form of tables in the database.
 * </p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public interface ProjectDao {

    /**
     * <p>
     * Inserts the object project into the project table.
     * </p>
     *
     * @param   project
     *          the project object that is to be inserted to the table.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean insertProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Deletes a project from the table.
     * </p>
     *
     * @param   project
     *          the project object that is to be deleted
     *     
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean deleteProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Updates the details of a project using project is as reference.
     * </p>
     *
     * @param   project
     *          the project object that is to be updated.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean updateProject(Project project) throws ApplicationException;

    /**
     * <p>
     * Returns all the projects currently present as a list.
     * </p>
     * 
     * @returns  projects
     *           the list containing all existing projects.
     */
    List<Project> getProjects() throws ApplicationException;

    /**
     * <p>
     * Retrives the project that corresponds to the given id.
     * </p>
     *
     * @param   id
     *          the id of the project to be pulled out.
     * @returns project
     *          the project object that corresponds to the given id.
     */
    Project retriveProjectById(int id) throws ApplicationException;
}
