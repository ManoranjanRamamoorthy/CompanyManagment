package com.ideas2it.address.service.impl;

import java.util.List;

import com.ideas2it.address.dao.AddressDao;
import com.ideas2it.address.dao.impl.AddressDaoImpl;
import com.ideas2it.address.model.Address;
import com.ideas2it.address.service.AddressService;
import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Implements AddressService Performs specific functions regarding the 
 * addition, updation, and deletion of Address details
 * </p>
 *
 * @created    12/09/2017
 * @author     Manoranjan.R
 */
public class AddressServiceImpl implements AddressService {
    private AddressDao addressDao;

    public void setAddressDao(AddressDao addressDao) {
        this.addressDao = addressDao;
    }

    /**
     * @see      com.ideas2it.address.service.AddressService
     * @method   createAddress
     */
     public Address createAddress(String doorNumber, String street,
       String district, String town, String state) throws ApplicationException {
        Address address = new Address(doorNumber, street, town, district,
                                                                         state);
        return address;
     }

    /**
     * @see      com.ideas2it.address.service.AddressService
     * @method   deleteAddress
     */
    public boolean deleteAddress(Address address) throws ApplicationException {
        address.setIsActive(Boolean.FALSE);
        return addressDao.deleteAddress(address);
    }
        
    /**
     * @see      com.ideas2it.address.service.AddressService
     * @method   updateAddress
     */
    public boolean updateAddress(Address address) throws ApplicationException {
        return addressDao.updateAddress(address);
    }

    /**
     * @see      com.ideas2it.address.service.AddressService
     * @method   getAllAddresses
     */
    public List<Address> getAllAddresses() throws ApplicationException {
        return addressDao.getAddresses();
    }
}
