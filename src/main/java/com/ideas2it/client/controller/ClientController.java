package com.ideas2it.client.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.address.model.Address;
import com.ideas2it.address.service.AddressService;
import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;

/**
 * <p>
 * The class ClientController stores, deletes, updates and displays client
 * details.
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R 
 */
@Controller
@RequestMapping("/ClientController")
public class ClientController extends HttpServlet {
    private ClientService clientService;
    private AddressService addressService;
    private static final String UPDATE = "/jsp/editClient.jsp";
    private static final String DELETE_OR_DISPLAY =
                                                 "/jsp/displayAllClients.jsp";
    private static final String INSERT = "/jsp/addClient.jsp";
    private static final String ASSIGN_PROJECT = "/jsp/assignProject.jsp";
    private static final String VIEW = "/jsp/viewClient.jsp";
    private static final AppLogger logger =
                              new AppLogger(ClientController.class.getName());

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public void setAddressService(AddressService addressService) {
        this.addressService = addressService;
    }

    /**
     * </p>
     * Enables deletion of a client
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_DELETE},
                                                    method = RequestMethod.POST)
    public ModelAndView deleteClient(@RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try {
            clientService.removeClient(id);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);            
             modelAndView.addObject(Constants.CLIENTS,
                                                clientService.retriveClients());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Updates the details stored in client object.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_EDIT},
                                                    method = RequestMethod.POST)
    public ModelAndView editClient(@RequestParam(Constants.ID) int id,
    @RequestParam(Constants.NAME) String name,
                                 @RequestParam(Constants.MAIL_ID) String email,
    @ModelAttribute(Constants.ADDRESS) Address address) {
        ModelAndView modelAndView = null;
        try {
            Client client = clientService.retriveClientById(id);
            client.setName(name);
            client.setEmail(email);
            List<Address> addresses = new ArrayList<>();
            addresses.add(address);
            client.setAddresses(addresses);
            clientService.modifyClient(client);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);            
             modelAndView.addObject(Constants.CLIENTS,
                                                clientService.retriveClients());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Gets the details of the client and performs
     * addition of an client
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_ADD},
                                                    method = RequestMethod.POST)
    public ModelAndView addClient(
                @ModelAttribute(Constants.ADDRESS) Address address,
                @ModelAttribute(Constants.CLIENT) Client client,
                @RequestParam(Constants.TEMP_DOOR) String doorNumber,
                @RequestParam(Constants.TEMP_STREET) String street,
                @RequestParam(Constants.TEMP_TOWN) String town,
                @RequestParam(Constants.TEMP_DISTRICT) String district,
                @RequestParam(Constants.TEMP_STATE) String state,
                @RequestParam(Constants.IS_DUAL) String choice
                ) {
        ModelAndView modelAndView = null;
        try {
            List<Address> addresses = new ArrayList<>();
            
            addresses.add(addressService.createAddress(doorNumber,
                                                street, town, district, state));
            if (choice.equals(Constants.YES)) {
                addresses.add(address);
            }
            client.setAddresses(addresses);
            clientService.createClient(client);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);            
             modelAndView.addObject(Constants.CLIENTS,
                                                clientService.retriveClients());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Updates the details stored in client object.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_UPDATE},
                                                    method = RequestMethod.POST)
    public ModelAndView updateClient(@RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try {
            Client client = clientService.retriveClientById(id);
            String forward = UPDATE;
            modelAndView = new ModelAndView(forward);            
             modelAndView.addObject(Constants.CLIENT, client);
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Displays all the clients that are currently active.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_DISPLAY},
                                                     method = RequestMethod.GET)
    public ModelAndView displayAllClients() {
        ModelAndView modelAndView = null;
        try {
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);            
             modelAndView.addObject(Constants.CLIENTS,
                                                clientService.retriveClients());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Gets the details of the client and performs
     * addition of an client
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_INSERT},
                                                    method = RequestMethod.POST)
    public ModelAndView insertClient() {
        ModelAndView modelAndView = null;
        try {
            String forward = INSERT;
            modelAndView = new ModelAndView(forward);  
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Adds one or many projects to a given client.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_ASSIGN_PROJECT},
                                                    method = RequestMethod.POST)
    public ModelAndView assignProjects(
                @RequestParam(Constants.PROJECT_ID) int projectId,
                @RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try {
            List<Integer> projectIds = new ArrayList<>();
            projectIds.add(projectId);
            clientService.addProjectsToClient(id, projectIds);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);            
            modelAndView.addObject(Constants.CLIENTS,
                                                clientService.retriveClients());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * recieves the request and redirects to the project assignment page.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_GET_PROJECT},
                                                    method = RequestMethod.POST)
    public ModelAndView getProject(@RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try {
            String forward = ASSIGN_PROJECT;
            modelAndView = new ModelAndView(forward);            
            modelAndView.addObject(Constants.CLIENT,
                                           clientService.retriveClientById(id));
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Removes a project from a client
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_REMOVE_PROJECT},
                                                    method = RequestMethod.POST)
    public ModelAndView removeProject(
                @RequestParam(Constants.ID) int id,
                @RequestParam(Constants.PROJECT_ID) int projectId) {
        ModelAndView modelAndView = null;
        try {
            clientService.removeProjectFromClient(id, projectId);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);            
             modelAndView.addObject(Constants.CLIENTS,
                                                clientService.retriveClients());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Views the Client and its details
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_VIEW},
                                                    method = RequestMethod.POST)
    public ModelAndView viewClient(@RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try {
            Client client = clientService.retriveClientById(id);
            String forward = VIEW; 
            modelAndView = new ModelAndView(forward);            
             modelAndView.addObject(Constants.CLIENT, client);
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }
}
