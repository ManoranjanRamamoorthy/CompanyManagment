package com.ideas2it.employee.model;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.login.model.Login;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * The class Employee has getters and setters that help 
 * to access the class members 
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */
public class Employee {
    private boolean isActive = Boolean.TRUE;
    private int id;
    private String dob;
    private String doj;
    private String email;
    private String name;
    private List<Address> addresses;
    private Login login;
    private Project project;

    public Employee() {
    }

    public Employee(String name, String email, String dob, String doj,
                                                      List<Address> addresses) {
        this.setAddresses(addresses);
        this.setDob(dob);
        this.setDoj(doj);
        this.setEmail(email);
        this.setName(name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

   public String getDoj() {
        return doj;
    }

    public void setDoj(String doj) {
        this.doj = doj;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }
    
    public void setProject(Project project) {
        this.project = project;
    }


    public Project getProject() {
        return this.project;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Login getLogin() {
        return this.login;
    }
}
