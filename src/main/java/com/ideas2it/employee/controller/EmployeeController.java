package com.ideas2it.employee.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.address.model.Address;
import com.ideas2it.address.service.AddressService;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;

/**
 * <p>
 * The class EmployeeController stores, deletes, updates and displays employee
 * details.
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R 
 */
@Controller
@RequestMapping("/EmployeeController")
public class EmployeeController extends HttpServlet {
    private EmployeeService employeeService;
    private AddressService addressService;

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public void setAddressService(AddressService addressService) {
        this.addressService = addressService;
    }
 
    private static final String UPDATE = "/jsp/editEmployee.jsp";
    private static final String DELETE_OR_DISPLAY =
                                                 "/jsp/displayAllEmployees.jsp";
    private static final String INSERT = "/jsp/addEmployee.jsp";
    private static final String VIEW = "/jsp/viewEmployee.jsp";
    
    private static final AppLogger logger =
                              new AppLogger(EmployeeController.class.getName());

    /**value = "/EmployeeController", 
     * <p>
     * deletes the Employee object using the id.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_DELETE},
                                                    method = RequestMethod.POST) 
    public ModelAndView deleteEmployee(@RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try {
            employeeService.removeEmployee(id);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.EMPLOYEES,
                                            employeeService.retriveEmployees());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Updates the Employee details as given by the user.
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_EDIT},
                                                    method = RequestMethod.POST)
    public ModelAndView editEmployee(@RequestParam(Constants.ID) int id,
            @RequestParam(Constants.NAME) String name,
            @RequestParam(Constants.MAIL_ID) String email,
            @RequestParam(Constants.DOB) String dob,
            @RequestParam(Constants.DOJ) String doj,
            @ModelAttribute(Constants.ADDRESS) Address address) {
        ModelAndView modelAndView = null;
        try{
            Employee employee = employeeService.retriveEmployeeById(id);
            employee.setName(name);
            employee.setEmail(email);
            employee.setDob(dob);
            employee.setDoj(doj);
            List<Address> addresses = new ArrayList<>();
            addresses.add(address);
            employee.setAddresses(addresses);
            employeeService.modifyEmployee(employee);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.EMPLOYEES,
                                            employeeService.retriveEmployees());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Displays all the employees that are currently active
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_DISPLAY},
                                                     method = RequestMethod.GET)
    public ModelAndView displayEmployees() {
        ModelAndView modelAndView = null;
        try{
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.EMPLOYEES,
                                            employeeService.retriveEmployees());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * Gets the details and adds them as a new Employee object
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_ADD},
                                                    method = RequestMethod.POST)
    public ModelAndView addEmployee(
                @ModelAttribute(Constants.EMPLOYEE) Employee employee,
                @ModelAttribute(Constants.ADDRESS) Address address,
                @RequestParam(Constants.TEMP_DOOR) String doorNumber,
                @RequestParam(Constants.TEMP_STREET) String street,
                @RequestParam(Constants.TEMP_TOWN) String town,
                @RequestParam(Constants.TEMP_DISTRICT) String district,
                @RequestParam(Constants.TEMP_STATE) String state) {
        ModelAndView modelAndView = null;

        try{
            List<Address> addresses = new ArrayList<>();
            addresses.add(address);
            addresses.add(addressService.createAddress(doorNumber, street,
                                                        town, district, state));
            employee.setAddresses(addresses);
            employeeService.createEmployee(employee);
            String forward = DELETE_OR_DISPLAY;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.EMPLOYEES,
                                            employeeService.retriveEmployees());
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * redirects the request to the respective insert employee page by setting
     * necessary parameters
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_INSERT},
                                                    method = RequestMethod.POST)
    public ModelAndView redirectToAddEmployee() {
        ModelAndView modelAndView = null;
        try{
            String forward = INSERT;
            modelAndView = new ModelAndView(forward);
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * redirects the request to the respective update employee page by setting
     * necessary parameters
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_UPDATE},
                                                    method = RequestMethod.POST)
    public ModelAndView redirectToEditEmployee(
                                           @RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try{
            Employee employee = employeeService.retriveEmployeeById(id);
            String forward = UPDATE;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.EMPLOYEE, employee);
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }

    /**
     * <p>
     * redirects the request to the respective view page,that displays the
     * Employee details by setting necessary parameters
     * </p>
     */
    @RequestMapping(params = {Constants.ACTION_VIEW},
                                                    method = RequestMethod.POST)
    public ModelAndView viewEmployee(@RequestParam(Constants.ID) int id) {
        ModelAndView modelAndView = null;
        try{
            Employee employee = employeeService.retriveEmployeeById(id);
            String forward = VIEW;
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject(Constants.EMPLOYEE, employee);
        } catch (Exception exception) {
            logger.logError(exception);
        } finally {
            return modelAndView;
        }
    }
}
