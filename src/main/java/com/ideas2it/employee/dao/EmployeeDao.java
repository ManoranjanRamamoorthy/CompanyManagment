package com.ideas2it.employee.dao;

import java.util.List;

import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * The interface EmployeeDao performs CRUD operations of Employee
 * using the Employees table in the database
 * </p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public interface EmployeeDao {

    /**
     * <p>
     * Inserts the object employee into the client table.
     * </p>
     *
     * @param   employee
     *          the employee object that is to be inserted to the table.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean insertEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * Deletes a employee from the table.
     * </p>
     *
     * @param   employee
     *          the employee who is to be deleted.
     *
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean deleteEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * Updates the details of a employee using employee id as reference.
     * </p>
     *
     * @param   employee
     *          the employee object that is to be updated.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean updateEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * Returns all the employees currently present as a list.
     * </p>
     * 
     * @returns  employees
     *           the list containing all existing employees.
     */
    List<Employee> getEmployees() throws ApplicationException;

    /**
     * <p>
     * Retrieves the Employee with the specified id from the table as an
     * Employee object.
     * </p>
     *
     * @param   id
     *          the employee's id who is to be pulled out.
     * @returns employee
     *          the employee object corresponding to the id.
     */
    Employee retriveEmployeeById(int id) throws ApplicationException;
}

