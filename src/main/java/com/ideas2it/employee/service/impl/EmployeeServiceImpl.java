package com.ideas2it.employee.service.impl;

import java.util.List;
import java.util.ArrayList;

import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.dao.EmployeeDao;
import com.ideas2it.employee.dao.impl.EmployeeDaoImpl;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.ProjectService;

/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of employee details
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeDao employeeDao;

    public void setEmployeeDao(EmployeeDao employeeDao) {
        this.employeeDao= employeeDao;
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  createEmployee
     */
    public boolean createEmployee(Employee employee)
                                                 throws ApplicationException {
        return employeeDao.insertEmployee(employee);
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  removeEmployee
     */
    public boolean removeEmployee(int employeeId) throws ApplicationException {
        Employee employee = employeeDao.retriveEmployeeById(employeeId);
        if (null != employee) {
            employee.setIsActive(Boolean.FALSE);
            employee.setProject(null);
            for (Address address : employee.getAddresses()) {
                address.setIsActive(Boolean.FALSE);
            }
            return employeeDao.deleteEmployee(employee);
        }
        return Boolean.FALSE;
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  modifyEmployee
     */
    public boolean modifyEmployee(Employee employee)
                                                   throws ApplicationException {
        return employeeDao.updateEmployee(employee);
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  retriveEmployeeById
     */
    public Employee retriveEmployeeById(int employeeId)
                                                   throws ApplicationException {
        return employeeDao.retriveEmployeeById(employeeId);
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  retriveEmployees
     */
    public List<Employee> retriveEmployees() throws ApplicationException {
        return employeeDao.getEmployees();
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  assignProjectToEmployee
     */
    public boolean assignProjectToEmployee(int employeeId, Project project)
                                                   throws ApplicationException {
        Employee employee = employeeDao.retriveEmployeeById(employeeId);
        if (null != employee) {
            employee.setProject(project);
            return employeeDao.updateEmployee(employee);
        }
        return Boolean.FALSE;
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  removeEmployeeFromProject
     */
    public boolean removeEmployeeFromProject(int id) throws
                                                         ApplicationException {
        Employee employee = employeeDao.retriveEmployeeById(id);
        if (null != employee) {
            employee.setProject(null);
            return employeeDao.updateEmployee(employee);
        }
        return Boolean.FALSE;
    }

    public List<Employee> retriveUnassignedEmployees() throws ApplicationException {
        List<Employee> employees = new ArrayList<>();
        for (Employee employee : this.retriveEmployees()) {
            if (null == employee.getProject()) {
                employees.add(employee);
            }
        }
        return employees;
    }
}
