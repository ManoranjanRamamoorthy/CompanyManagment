package com.ideas2it.filter;

import java.io.IOException;  
import java.io.PrintWriter;  
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AuthenticationFilter implements Filter{

    public void init(FilterConfig arg0) throws ServletException {
    }

    public void doFilter(ServletRequest req, ServletResponse res,  
                 FilterChain filterChain) throws IOException, ServletException {  
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session =  request.getSession(false);
        if((null != session) && (null != session.getAttribute("user"))) {
            filterChain.doFilter(request, response);
        } else {
            request.getSession().setAttribute("Msg", "Login to continue");
            response.sendRedirect("/CompanyManagement/index.jsp");
        }
    }  
    
    public void destroy() {
    }  
  
}  

