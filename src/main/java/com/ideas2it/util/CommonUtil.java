package com.ideas2it.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * class CommonUtil performs simple tasks that are commonly used
 * </p>
 *
 * @created    07/07/2017
 * @author     Manoranjan.R
 */
public class CommonUtil {
    private static final String emailPattern = (new StringBuilder("")
                                           .append("^[(a-zA-Z-0-9-\\_\\+\\.)]")
                       .append("+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$").toString());
     
    /**
     * <p>
     * Checks whether the date is in correct format
     * </p>
     *
     * @param    date that is to be validated  
     * 
     * @returns    TRUE if date is valid \ FALSE if date is not valid
     * </p>
     */
    public static boolean isValidDate(String dateString) {                      
        try {    
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
            Date date = dateFormat.parse(dateString);
            dateFormat.setLenient(Boolean.FALSE);
            return Boolean.TRUE;
        } catch (ParseException exception) {
            return Boolean.FALSE;
        }
    }

    /**
     * <p>
     * Validates the E-mail address
     * </p>
     * 
     * @param    email address
     *           that is to be validated
     *
     * @returns TRUE if  valid \ FALSE if  invalid
     */
    public static boolean isValidEmailAddress(String emailAddress) {
        Pattern pattern = Pattern.compile(emailPattern);
        Matcher matcher = pattern.matcher(emailAddress);   
        return matcher.matches();
    }
    
    /**
     * <p>
     * Converts the given String to Date format
     * </p>
     *
     * @param    dateString
     *           which is the string to be converted
     * @returns    date in Date format
     */
    public static Date convertStringToDate(String dateString) {                      
        try {    
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
            Date date = dateFormat.parse(dateString);
            dateFormat.setLenient(Boolean.FALSE);
            return date;
        } catch (ParseException exception) {
            return null;
        }
    }
   
}                        


