package com.ideas2it.util;

import org.hibernate.cfg.Configuration;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;
 
/**
 * This is a utility class for getting the SessionFactory instance for a
 * a paticular session.
 */
public class SessionUtil {
    private static SessionFactory sessionFactory;
    private static SessionUtil sessionUtil;
    private static final AppLogger logger =
                               new AppLogger(SessionUtil.class.getName());

    private SessionUtil() {
    }

    /**
     * <p>
     * Returns the instance of the singleton class
     * </p>
     *
     * @returns    sessionUtil
     *             the instance of the singleton class SessionUtil.
     */
    public static SessionUtil getInstance() {
        if (null == sessionUtil) {
            sessionUtil = new SessionUtil();
        }
        return sessionUtil;
    }

    /**
     * <p>
     * This method is used to get the instance of the session factory.
     * </p>
     *
     * @returns     session
     *              the Session instance for the given configuration.
     */

    public static Session getSession() throws ApplicationException {
        SessionFactory sessionFactory = null;
        try {
            Configuration configuration = new Configuration();
            configuration.configure(Constants.HIBERNATE_CFG);
            sessionFactory = configuration.buildSessionFactory();
            logger.logInfo(Constants.CONNECTION_ESTABLISHED);
        }
        catch (Exception exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.CONNECTING).toString());
        } finally {
            return sessionFactory.openSession();
        }
    }
}
