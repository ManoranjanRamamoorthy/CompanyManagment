package com.ideas2it.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * <p>
 * DateUtil performs some operations involving dates
 * </p>
 */
public class DateUtil {

    /**
     * <p>
     * Calculates the difference in years between the given dates
     * </p>
     *
     * @param   fromDate
     *          the date from which difference is to be calculated
     * @param   toDate
     *          the date till which the difference is to be calculated.
     *
     * @returns yearDifference
     *          the difference between the dates in years.
     */
    public static int calculateDifferenceInDates(Calendar fromDate,
            Calendar toDate) {
        int yearDifference = toDate.get(Calendar.YEAR)
                                                - fromDate.get(Calendar.YEAR);
        if (toDate.get(Calendar.MONTH) < fromDate.get(Calendar.MONTH)) {
            yearDifference--;
        } else {
            if (toDate.get(Calendar.MONTH) == fromDate.get(Calendar.MONTH)
                    && toDate.get(Calendar.DAY_OF_MONTH) <
                            fromDate.get(Calendar.DAY_OF_MONTH)) {
                yearDifference--;
            }
        }
        return yearDifference;
    }
}
