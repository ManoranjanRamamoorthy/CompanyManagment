package com.ideas2it.connectionfactory;

import java.lang.ClassNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLException;
import java.sql.Statement;

import com.ideas2it.common.Constants;

/**
 * <p>
 * Performs actions like establishing and closing connections with the database.
 *</p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public class ConnectionFactory {
    private static ConnectionFactory connectionFactory;
    private static Connection connection;

    private ConnectionFactory() {
    }

    /**
     * <p>
     * Returns the instance of the singleton class
     * </p>
     *
     * @returns    connectionFactory
     *             the instance of the singleton class ConnectionFactory.
     */
    public static ConnectionFactory getInstance() {
        if (null == connectionFactory) {
            connectionFactory = new ConnectionFactory();
        }
        return connectionFactory;
    }

    /**
     * <p>
     * Establishes connection with the Database
     * </p>
     *
     * @returns     connection
     *              the connection object that connects to the database.
     */
    public Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:" +
                                    "3306/CompanyManagement", "root", "ubuntu");
        } catch (SQLException exception) {
            System.out.println(Constants.CANT_OPEN);
        } catch (ClassNotFoundException exception) {
            System.out.println(Constants.CANT_OPEN);
        }
        return connection;
    }

    /**
     * <p>
     * Closes the connection that has been established
     * </p>
     */
    public void closeConnection() {
        if (null != connection) {
            try {
                connection.close();
            } catch (SQLException exception) {
            System.out.println(Constants.CANT_CLOSE);
            }
        }
    }
}
