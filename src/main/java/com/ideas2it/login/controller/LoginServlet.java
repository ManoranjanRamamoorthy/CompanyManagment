package com.ideas2it.login.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.login.service.LoginService;
import com.ideas2it.login.service.impl.LoginServiceImpl;
import com.ideas2it.login.model.Login;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.client.model.Client;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.client.service.impl.ClientServiceImpl;
import com.ideas2it.exception.ApplicationException;

@Controller
@RequestMapping("/LoginServlet")
public class LoginServlet extends HttpServlet {
    LoginService loginService;
    EmployeeService employeeService;
    ClientService clientService;

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }

    @RequestMapping(params = {"action=login"}, method = RequestMethod.POST)
    public ModelAndView login(HttpServletRequest request)
                                          throws ServletException, IOException {
        String message = "Incorrect Login credentials!!";
        String forward = "/index.jsp";
        ModelAndView modelAndView = null;
        try {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            List<Login> logins = loginService.retriveLogins();
            for (Login login : logins) {
                if ((login.getUsername()).equals(username) &&
                                       (login.getPassword()).equals(password)) {
                    HttpSession session = request.getSession(Boolean.TRUE);
                    session.setAttribute("user",username);
                    if (username.equals("admin")) {
                        forward = "/jsp/welcome.jsp";
                        message="Hi admin";
                    } else {
                        if (login.getIsEmployee()) {
                            modelAndView.addObject("login", login);
                            forward = "/jsp/viewEmployeeDetails.jsp";
                            message = "";
                        } else {
                            modelAndView.addObject("login", login);
                            forward = "/jsp/viewClientDetails.jsp";
                            message = "";
                        }
                    }
                }
            }
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject("msg", message);
        } catch (Exception exception) {
            System.out.println(exception);
            exception.printStackTrace();
        } finally {
            return modelAndView;
        }
    }

    @RequestMapping(params = {"action=signUp"}, method = RequestMethod.POST)
    public ModelAndView signUp(@RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam("userType") String userType,
            @RequestParam("id") int id) throws Exception, ApplicationException {
        ModelAndView modelAndView = null;
        String forward = "/index.jsp";
        String message = "";
        try {
            if (isValidUsername(username)) {
                Login login = new Login(username, password);
                if (userType.equals("client")) {
                    login.setIsEmployee(Boolean.FALSE);
                    Client client = clientService.retriveClientById(id);
                    if (null != client) {
                        login.setClient(client);
                        loginService.createLogin(login);
                        message = "Sign up complete. You may login now.";
                    } else {
                        message = "No client found with the given Id";
                    }
                } else {
                    Employee employee = employeeService.retriveEmployeeById(id);
                    if (null != employee) {
                        login.setIsEmployee(Boolean.FALSE);
                        login.setEmployee(employee);
                        loginService.createLogin(login);
                        message = "Sign up complete. You may login now.";
                    } else {
                        message = "No Employee found with the given Id";
                    }
                }
            } else {
                message = "Sorry, The username is unavailable";
            }
            modelAndView = new ModelAndView(forward);
            modelAndView.addObject("message", message);
            modelAndView.addObject("user", null);
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        } finally {
            return modelAndView;
        }
    }

    public boolean isValidUsername(String username)
                                                   throws ApplicationException {
        boolean isValid = Boolean.TRUE;
        List<Login> logins = loginService.retriveLogins();
        for (Login login : logins) {
            if (username == login.getUsername()) {
                isValid = Boolean.FALSE;
            }
        }
        return isValid;
    }
}

