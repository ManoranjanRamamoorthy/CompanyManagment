<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<style>
* {
    box-sizing: border-box;
}

input[type=text], select, textarea{
    width: 50%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

input[type=email], select, textarea{
    width: 50%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #333;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: black;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col {
    float: left;
    width: 55%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}

* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('');
  background-position: 10px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 18px;
}

#myTable th{
  text-align: center;
  padding: 12px;
  background-color:#ddd;
}
 #myTable td {
  text-align: center;
  padding: 12px;
  
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
.center{
float:center;
margin-left:35%;
}
</style>
    <head>
        <title>Client details</title>
    </head>
    <body><br><br><br><br>
        
        <form method="POST" action='ClientController' name="formEditClient">
            <input type="hidden" name="action" value="edit" >
            <div class="container">
            <fieldset>
                <legend><h2><b>Client Details</b></h2></legend>
                <div class="row">
                    <div class="col-25">Client ID</div>
                    <div class="col-75"><input type="text" name="id" readonly="readonly" value="${client.id}"></div>
                </div>
                <div class="row">
                    <div class="col-25">Name</div>
                    <div class="col-75"><input type="text" name="name" readonly="readonly"maxlength="20" value="${client.name}" ></div>
                </div>
                <div class="row">
                    <div class="col-25">Email Address</div>
                    <div class="col-75"><input type="email" name="email" readonly="readonly"value="${client.email}" ></div>
                </div>


<c:set var="addressType" value="Permanent"></c:set>
<c:forEach items="${client.addresses}" var="address">
<div class="container">
<fieldset>
                    <legend><h2>${addressType} Address :</h2></legend>
                    <c:set var="addressType" value="Temporary"></c:set>
    <div class="row">
        <div class="col-25">
        Door Number:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="doorNumber" readonly="readonly"value="${address.doorNumber}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Street:
        </div>
        <div class="col-75">
        <input type="text" maxlength="10" name="street" readonly="readonly"value="${address.street}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Town:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="town" readonly="readonly" value="${address.town}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        District::
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="district" readonly="readonly" value="${address.district}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        State:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="state" readonly="readonly" value="${address.state}" />
        </div>
    </div>
</div>
</c:forEach>


<h4>Projects:<h4><br>
        <table id="myTable">
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Domain</th>
                <th>Description</th>
            </tr>
            <c:forEach items="${client.projects}" var="project">
                <tr>
                    <td>${project.id}</td>
                    <td>${project.title}</td>
                    <td>${project.domain}</td>
                    <td>${project.description}</td>
                </tr>
            </c:forEach>
        </table>
            </fieldset>
        </form>
        </fieldset>
    </body>
</html>
