<%@ include file="header.jsp" %>

<html>
<style>

* {
    box-sizing: border-box;
}

input[type=text], select, textarea{
    width: 50%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

input[type=email], select, textarea{
    width: 50%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #333;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: black;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col {
    float: left;
    width: 55%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}

tr,td,th {
    width:75%;
    }
</style>
    <head>
        <title>Add New Client</title>
    <script>
    
        function enable()
        {
            
        if (document.getElementById('isHavingTwoAddresses').disable == true)
          {
	        document.getElementById('doorNumber').disabled = true;
	        document.getElementById('street').disabled = true;
	        document.getElementById('town').disabled = true;
	        document.getElementById('district').disabled = true;
	        document.getElementById('state').disabled = true;
         }
        else
          {
           	document.getElementById('doorNumber').disabled = false;
           	document.getElementById('street').disabled = false;
           	document.getElementById('town').disabled = false;
           	document.getElementById('district').disabled = false;
           	document.getElementById('state').disabled = false;
          }
          getAnswer("no");
        }

        function disable()
        {
            
         document.getElementById('doorNumber').disabled = true;
	     document.getElementById('street').disabled = true;
	     document.getElementById('town').disabled = true;
	     document.getElementById('district').disabled = true;
	     document.getElementById('state').disabled = true;
	     getAnswer("no");
        }
    </script>
    </head>
    
    <body><br><br><br><br>
    <h2>Add New Client</h2>
        <div class="container">
  <form action='ClientController' method="post">
    <input type="hidden" name="action" value="add">
    <div class="row">
      <div class="col-25">
        Name:
      </div>
      <div class="col-75">
        <input type="text" name="name" required placeholder="client name...">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        E-mail Address:
      </div>
      <div class="col-75">
        <input type="email" name="email" required placeholder="(eg:xxxxxx@gmail.com)">
      </div>
    </div>




<b>Permanent Address:</b>
<div class="container">
    <div class="row">
      <div class="col-25">
        Door Number:
      </div>
      <div class="col-75">
        <input type="text" name="temp_doorNumber" id="doorNumber1" placeholder="door number" required>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        Street:
      </div>
      <div class="col-75">
        <input type="text" name="temp_street" id="street1" placeholder="street" required>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        Town:
      </div>
      <div class="col-75">
        <input type="text" name="temp_town" id="town1" placeholder="town" required>
      </div>
    <div class="row">
      <div class="col-25">
        District::
      </div>
      <div class="col-75">
        <input type="text" name="temp_district" id="district1" placeholder="district" required>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        State:
      </div>
      <div class="col-75">
        <input type="text" name="temp_state" id="state1" placeholder="state" required>
      </div>
    </div>
    </div>
</div>

Would you like to add a temporary address: <input type= "radio"  id= "isHavingTwoAddresses" name="isHavingTwoAddresses"  value="yes" onclick="javascript:enable();">Yes
<input type= "radio"  id= "isHavingTwoAddresses" name="isHavingTwoAddresses"  value="no" onclick="javascript:disable();">No<br><br><br>

    <b>Temporary Address:</b>
    <div class="container">
        <div class="row">
          <div class="col-25">
            Door Number:
          </div>
          <div class="col-75">
            <input type="text" name="doorNumber" id="doorNumber" placeholder="door number" disabled >
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            Street:
          </div>
          <div class="col-75">
            <input type="text" name="street" id="street" placeholder="street" disabled>
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            Town:
          </div>
          <div class="col-75">
            <input type="text" name="town" id="town" placeholder="town" disabled>
          </div>
        <div class="row">
          <div class="col-25">
            District::
          </div>
          <div class="col-75">
            <input type="text" name="district" id="district" placeholder="district" disabled>
          </div>
        </div>
        <div class="row">
          <div class="col-25">
            State:
          </div>
          <div class="col-75">
            <input type="text" name="state" id="state" placeholder="state" disabled>
          </div>
        </div>
        </div>
    </div>

   <div class="row">
      <div class="col">
        <input type="submit" class="w3-btn w3-teal" value="Submit" />
      </div>
    </div>
    </div>
        </form>
    </body>
</html>
