<html>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body >
<style>
.navbar {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  top: 0;
  width: 100%;
  height:70px;
}

.navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 25px 16px;
  text-decoration: none;
  font-size: 17px;
}

</style>
<div class="navbar">
  <a href="./EmployeeController?action=display" class="w3-bar-item w3-button">Employee Management</a>
  <a href="./ProjectController?action=display" class="w3-bar-item w3-button">Project Management</a>
  <a href="./ClientController?action=display" class="w3-bar-item w3-button">Client Management</a>
  <a class="w3-bar-item w3-button w3-red w3-right" onclick="return confirm('You will be logged out.\nPress OK to continue');" href="./LogoutServlet"/>Log Out</a>
</div>
</body>
</html>
