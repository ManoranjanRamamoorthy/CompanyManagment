<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<style>
* {
    box-sizing: border-box;
}

input[type=text], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

input[type=email], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #333;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: black;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

.col-60 {
    float: left;
    width: 48.5%;
    margin-top: 6px;
}

.col-50 {
    float: center;
    width: 50%;
    margin-top: 6px;
}

.col-10 {
    float: left;
    width: 10%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}
</style>
    <head>
        <title>Edit Project</title>
    </head>
    <body><br><br><br><br>
        <form method="POST" action='ProjectController' name="formEditProject">
            <input type="hidden" name="action" value="edit" />
            <div class="container">
            <h2>Update Project Deatils</h2>
                <div class="row">
                    <div class="col-25">Project ID</div>
                    <div class="col-75"><input type="text" name="id" readonly="readonly" value="${project.id}" ></div>
                </div>
                <div class="row">
                    <div class="col-25">Title</div>
                    <div class="col-75"><input type="text" name="title" maxlength="20" value="${project.title}" /></div>
                </div>
                <div class="row">
                    <div class="col-25">Domain</div>
                    <div class="col-75"><input type="text" name="domain" maxlength="20" value="${project.domain}" /></div>
                </div>
                <div class="row">
                    <div class="col-25">Description</div>
                    <div class="col-75"><textarea type="text" name="description" row="10" cols="50">${project.description}</textarea></div>
                </div>
                <div class="row">
                    <div class="col-25"></div>
                    <div class="col-50"><input  class="w3-btn w3-teal" type="submit" value="Submit" /></div>
                </div>
            </div>
        </form>
    </body>
</html>
