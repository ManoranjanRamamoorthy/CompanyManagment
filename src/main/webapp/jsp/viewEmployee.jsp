<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<style>
* {
    box-sizing: border-box;
}
input[type=text], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

input[type=email], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #333;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: black;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}
a {
    display: block;
    color: white;
    text-align: center;
    padding: 20px 16px;
    text-decoration: none;
}

a:hover {
    background-color: #111;
}
}
.center{
float:left;
margin-left:45%;
}

</style>
    <head>
        <title>Employee details</title>
    </head>
    <body><br><br><br><br>
    <fieldset><legend><h2> Employee Deatils</h2></legend>
<form method="POST" action='EmployeeController' name="formEditEmployee">
<div class="container">
    <input type="hidden" name="action" value="edit" />
    <div class="row">
        <div class="col-25">
        Id:
        </div>
        <div class="col-75">
        <input type="text" name="id" readonly="readonly" value="${employee.id}" >
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Name:
        </div>
        <div class="col-75">
        <input type="text" name="name"readonly="readonly"  maxlength="20" value="${employee.name}" required>
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Date Of Birth:
        </div>
        <div class="col-75">
        <input value="${employee.dob}" type="text" size="12"readonly="readonly" 
                                 required pattern="\d{1,2}/\d{1,2}/\d{4}"
                             placeholder="dd/mm/yyyy" required name="dob">
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Date Of Joining:
        </div>
        <div class="col-75">
        <input value="${employee.doj}" type="text" size="12"readonly="readonly" 
                                 required pattern="\d{1,2}/\d{1,2}/\d{4}"
                         placeholder="dd/mm/yyyy" required name="doj">
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Email Address:
        </div>
        <div class="col-75">
        <input type="email" name="email" readonly="readonly" value="${employee.email}" required />
        </div>
    </div>
</div>

<c:set var="addressType" value="Permanent"></c:set>
                <c:forEach items="${employee.addresses}" var="address">
<fieldset><legend><h2>${addressType} Address:</h2></legend>
<div class="container">
                    
                    <c:set var="addressType" value="Temporary"></c:set>
    <div class="row">
        <div class="col-25">
        Door Number:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" readonly="readonly" name="doorNumber" value="${address.doorNumber}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Street:
        </div>
        <div class="col-75">
        <input type="text" maxlength="10" readonly="readonly" name="street" value="${address.street}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Town:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" readonly="readonly" name="town" value="${address.town}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        District::
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" readonly="readonly" name="district" value="${address.district}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        State:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" readonly="readonly" name="state" value="${address.state}" />
        </div>
    </div>
</div>
</fieldset>
</c:forEach>
</form>
</filedset><br><br><br>
</body>
</html>
