<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('images/search.png');
  background-position: 0px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
  background-size: 42px 35px;
  margin-left:5%;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 18px;
}

#myTable th{
  text-align: center;
  padding: 12px;
  background-color:#ddd;
}
 #myTable td {
  text-align: center;
  padding: 12px;
  
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
.center{
float:center;
margin-left:35%;
}
</style>
    <head>
        <title>All Projects</title>

            <script>
            function myFunction() {
              var input, filter, table, tr, td, i;
              input = document.getElementById("myInput");
              filter = input.value.toUpperCase();
              table = document.getElementById("myTable");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }       
              }
            }
            </script>

    </head>
    <body ><br><br><br><br><br><br>
     <div class="center"><h2> Project Management</h2></div>
     <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Project titles.." title="Type in a name">
        <table id="myTable">
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Domain</th>
                <th>Description</th>
                <th  colspan=3 >Action</th>
            </tr>
            <c:forEach items="${projects}" var="project">
                <tr>
                    <td>${project.id}</td>
                    <td>${project.title}</td>
                    <td>${project.domain}</td>
                    <td>${project.description}</td>
                    <td>
                        <form method="POST" action="ProjectController">
                            <input type="hidden" name="action" value="update" />
                            <input type="hidden" name="id" value="${project.id}" />
                            <input class="w3-btn w3-teal" type="submit" value="Update" />
                        </form></td>
                    <td>
                        <form method="POST" action="ProjectController">
                            <input type="hidden" name="action" value="getEmployee" />
                            <input type="hidden" name="id" value="${project.id}" />
                            <input class="w3-btn w3-teal" type="submit" value="Manage Employees in this project" />
                        </form></td>
                    <td>
                        <form method="POST" action="ProjectController">
                            <input type="hidden" name="action" value="delete" />
                            <input type="hidden" name="id" value="${project.id}" />
                            <input class="w3-btn w3-teal" type="submit" value="remove" onclick="return confirm('The Project will be removed permanently.\nAre you sure you want to continue?');"/>
                        </form></td>    
                
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
