<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page session = "false" %>
<html>
<style>
a {
    display: block;
    color: #333;
    text-align: center;
    padding: 20px 16px;
    text-decoration: none;
    margin-left:10%;
    margin-right:10%;
}

a:hover {
    background-color: #16A085;
    color:white;
}
.center {
    float:left;
    margin-left:20%;
}
.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
    margin-left:30%;
    margin-right:30%;
    margin-top:5%;
}
</style>

<body>
<br><br><br>
<div class="center">Hi admin</div>
<div class="container">
<center><u><h2>WELCOME</h2></u></center>
<a href="./EmployeeController?action=display"/>Employee Management</a><br>
<a href="./ProjectController?action=display"/>Project Management</a><br>
<a href="./ClientController?action=display"/>Client Management</a><br>
</div>
</body>
</html>
