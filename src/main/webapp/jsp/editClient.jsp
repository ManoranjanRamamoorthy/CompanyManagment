<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<style>
* {
    box-sizing: border-box;
}

input[type=text], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

input[type=email], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #333;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: black;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}
.col-60 {
    float: left;
    width: 48.5%;
    margin-top: 6px;
}
.col-15 {
    float: left;
    width: 5%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}

.col {
    float: left;
    width: 55%;
    margin-top: 6px;
}


</style>
    <head>
        <title>Edit Client</title>
    </head>
    <body><br><br><br><br>
        <form method="POST" action='ClientController' name="formEditClient">
            <input type="hidden" name="action" value="edit" >
            <div class="container">
                <h2><b>Update Client Details</b></h2>
                <div class="row">
                    <div class="col-25">Client ID</div>
                    <div class="col-75"><input type="text" name="id" readonly="readonly" value="${client.id}"></div>
                </div>
                <div class="row">
                    <div class="col-25">Name</div>
                    <div class="col-75"><input type="text" name="name" required maxlength="20" value="${client.name}" ></div>
                </div>
                <div class="row">
                    <div class="col-25">Email Address</div>
                    <div class="col-75"><input type="email" name="email" required value="${client.email}" ></div>
                </div>
           </div>


<c:set var="addressType" value="Permanent"></c:set>
<c:forEach items="${client.addresses}" var="address">

<div class="container">
                    <h4>${addressType} Address </h4>
                    <c:set var="addressType" value="Temporary"></c:set>
    <div class="row">
        <div class="col-25">
        Door Number:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="doorNumber" value="${address.doorNumber}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Street:
        </div>
        <div class="col-75">
        <input type="text" maxlength="10" name="street" value="${address.street}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        Town:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="town"  value="${address.town}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        District::
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="district"  value="${address.district}" />
        </div>
    </div>
    <div class="row">
        <div class="col-25">
        State:
        </div>
        <div class="col-75">
        <input type="text" maxlength="20" name="state"  value="${address.state}" />
        </div>
    </div>
</div>
</c:forEach>

           <div class="container">
                <div  class="row">
                    <div class="col"v><input class="w3-btn w3-teal" type="submit" value="Submit" ></div>
                </div>
            </div>
        </form>
    </body>
</html>
