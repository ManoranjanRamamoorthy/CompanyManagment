<!DOCTYPE html>
<%@ include file="header.jsp" %>
<html>
<head>
<style>

input[type=text], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #333;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: black;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

.col {
    float: left;
    width: 55%;
    margin-top: 6px;
}

</style>
</head>
<body><br><br><br><br>

<h2>Add a new Project</h2>

<div class="container">
  <form action='ProjectController' method="post">
    <input type="hidden" name="action" value="add">
    <div class="row">
    <div class="col-25">
    client Id:
    </div>
    <div class="col-75">
    <input type="text"  readonly="readonly" name="clientId" value="${clientId}">
    </div>
    </div>
    <div class="row">
      <div class="col-25">
        Title:
      </div>
      <div class="col-75">
        <input type="text" name="title" required placeholder="project title...">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        Domain:
      </div>
      <div class="col-75">
        <input type="text" name="domain" required placeholder="project domain...">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
          Description:
      </div>
      <div class="col-75">
        <textarea type="text" name="description" required placeholder="project description..." style="height:200px"></textarea>
      </div>
    </div>
    <div class="row">
    <div class="col">
      <input type="submit" class="w3-btn w3-teal" value="Submit">
    </div></div>
  </form>
</div>

</body>
</html>
