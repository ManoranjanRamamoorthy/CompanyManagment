<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<style>
* {
  box-sizing: border-box;
}

#myInput {
  background-image: url('images/search.png');
  background-position: 0px 10px;
  background-repeat: no-repeat;
  width: 100%;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
  background-size: 42px 35px;
  margin-left:5%;
}

#myTable {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid #ddd;
  font-size: 18px;
}

#myTable th{
  text-align: center;
  padding: 12px;
  background-color:#ddd;
}
 #myTable td {
  text-align: center;
  padding: 12px;
  
}

#myTable tr {
  border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
  background-color: #f1f1f1;
}
.center{
float:center;
margin-left:35%;
}
</style>
    <head>
        <title>All Clients</title>
            <script>
            function myFunction() {
              var input, filter, table, tr, td, i;
              input = document.getElementById("myInput");
              filter = input.value.toUpperCase();
              table = document.getElementById("myTable");
              tr = table.getElementsByTagName("tr");
              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }       
              }
            }
            </script>
    </head>
    <body><br><br><br><br>
<br>
        <form method="POST" action="ClientController">
            <input type="hidden" name="action" value="insert" />
            <input type="submit" class="w3-btn w3-teal" value="Add New Client" />
        </form>
                <div class="center"><h2> Client Management</h2></div>
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for Client names.." title="Type in a name">
        <table id="myTable">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email Address</th>
                <th colspan=4>Action</th>
            </tr>
            <c:forEach items="${clients}" var="client">
                <tr>
                    <td >${client.id}</td>
                    <td >${client.name}</td>
                    <td >${client.email}</td>
                    <td >
                        <form method="POST" action="ClientController">
                            <input type="hidden" name="action" value="update" />
                            <input type="hidden" name="id" value="${client.id}" />
                            <input class="w3-btn w3-teal" type="submit" value="Update" />
                        </form></td>
                    <td>
                        <form method="POST" action="ClientController">
                            <input type="hidden" name="action" value="view" />
                            <input type="hidden" name="id" value="${client.id}" />
                            <input class="w3-btn w3-teal" type="submit" value="View" />
                        </form></td>
                    <td>
                        <form method="POST" action="ClientController">
                            <input type="hidden" name="action" value="delete" />
                            <input type="hidden" name="id" value="${client.id}" />
                             <input class="w3-btn w3-teal" type="submit" value="Delete" onclick="return confirm('All the details of this Client (${client.name}) will be removed.\nAre you sure you want to delete ?')" />
                        </form></td>
                    <td>
                        <form method="POST" action="ProjectController">
                            <input type="hidden" name="action" value="insert" />
                            <input type="hidden" name="clientId" value="${client.id}" />
                            <input class="w3-btn w3-teal" type="submit" value="Add Project" />
                        </form></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
