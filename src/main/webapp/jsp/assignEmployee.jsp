<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<style>
* {
    box-sizing: border-box;
}

input[type=text], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

input[type=email], select, textarea{
    width: 75%;
    padding: 12px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    resize: vertical;
}

label {
    padding: 12px 12px 12px 0;
    display: inline-block;
}

input[type=submit] {
    background-color: #333;
    color: white;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    float: right;
}

input[type=submit]:hover {
    background-color: black;
}

.container {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

.col-25 {
    float: left;
    width: 25%;
    margin-top: 6px;
}

.col-75 {
    float: left;
    width: 75%;
    margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}

.row1 {
    content: "";
    width:100%;
    display: table;
    clear: both;
}
/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media (max-width: 600px) {
    .col-25, .col-75, input[type=submit] {
        width: 100%;
        margin-top: 0;
    }
}

.col {
    float: left;
    width: 55%;
    margin-top: 6px;
}
</style>
    <head>
        <title>Assign Employee</title>
    </head>
    <body><br><br><br><br>
    <table style="width:100%">
        <tr>
        <th colspan="2">
        <h2>Project : ${project.title}</h2>
        <tr class="row1">
        <td>
        <form method="POST" action='ProjectController' name="formEditProject">
            <input type="hidden" name="action" value="assignEmployees" />
            <input type="hidden" name="id" value="${project.id}">
            <div class="container">
                <div class="row">
                    <div class="col-25">Assign an Employee</div>
                    <select required name="employeeId" style="width:60%">
                    <option value=""> Select an Employee</option>
                    <c:forEach items="${employees}" var="employee">
                    <option  value="${employee.id}">${employee.id} - ${employee.name}</option>
                    </c:forEach>
                    </select>
                </div>
                <div class="row">
                    <div class="col"><input  class="w3-btn w3-teal" type="submit" value="Assign" /></div>
                </div>
            </div>
            </td>
            <td>
        </form>
                <form method="GET" action='ProjectController' name="deAssign">
            <input type="hidden" name="action" value="deAssignEmployee" />
            <input type="hidden" name="id" value="${project.id}">
            <div class="container">
                <div class="row">
                    <div class="col-25">De-Assign an Employee</div>
                    <select required name="employeeId" style="width:60%">
                    <option value=""> Select an Employee</option>
                    <c:forEach items="${project.employees}" var="employee">
                    <option  value="${employee.id}">${employee.id} - ${employee.name}</option>
                    </c:forEach>
                    </select>
                </div>
                <div class="row">
                    <div class="col"><input class="w3-btn w3-teal" type="submit" value="De-assign" onclick="return confirm('The Employee will be removed from this Project (${project.title})\nAre you sure you want to continue?');"/></div>
                </div>
            </div>
        </form>
        </td>
        </tr>
    </body>
</html>
