package com.ideas2it.common;
/**
 * <p>
 * The Constants class is used to store and access some string and integer
 * constants that are frequently used.
 * </p>
 */
public class Constants {
    public static final int EIGHT = 8;
    public static final int FIVE = 5;
    public static final int FOUR = 4;
    public static final int MIN_AGE = 18;
    public static final int NINE = 9;
    public static final int ONE = 1;
    public static final int SEVEN = 7;
    public static final int SIX = 6;
    public static final int THREE = 3;
    public static final int TWO = 2;
    public static final int ZERO = 0;
    public static final String ACTION_ADD = "action=add";
    public static final String ACTION_ASSIGN_EMPLOYEES = "action=assignEmployees";
    public static final String ACTION_DEASSIGN_EMPLOYEE = "action=deAssignEmployee";
    public static final String ACTION_DELETE = "action=delete";
    public static final String ACTION_DISPLAY = "action=display";
    public static final String ACTION_EDIT = "action=edit";
    public static final String ACTION_GET_EMPLOYEE = "action=getEmployee";
    public static final String ACTION_INSERT = "action=insert";
    public static final String ACTION_UPDATE = "action=update";
    public static final String ACTION_VIEW = "action=view";
    public static final String ADDED = "Details Added";
    public static final String ADDING = "adding ";
    public static final String ADDRESS = "address";
    public static final String ADDRESSES = "ADDRESSES : ";
    public static final String ADDRESS_ID = "Address Id: ";
    public static final String AGE_INVALID = "Minimum age is 18. Enter DOB";
    public static final String ASSIGNING = " assigning ";
    public static final String CANCELLED = "Operation Cancelled";
    public static final String CANT_CLOSE = " Closing connection failed ";
    public static final String CANT_OPEN = " Opening connection failed ";
    public static final String CLIENT = "client";
    public static final String CLIENT_ID = "clientId";
    public static final String CLIENTS = "clients";
    public static final String CONFIG_LOG = "com/ideas2it/logger/log4j.xml";
    public static final String CONNECTING = " connecting";
    public static final String CONNECTION_ESTABLISHED = (new StringBuilder("")
                                .append("Connection established").toString());
    public static final String DEASSIGNING = " deassigning ";
    public static final String DELETED = "\nDeleted";
    public static final String DELETING = "deleting ";
    public static final String DESCRIPTION = "descripton";
    public static final String DISTRICT = "district";
    public static final String DOB = "dob";
    public static final String DOJ = "doj";
    public static final String DOMAIN = "domain";
    public static final String DOOR_NO = "doorNumber";
    public static final String EMPLOYEES = "employees";
    public static final String EMPLOYEE = "employee";
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String FAILED = "Operation Failed !!";
    public static final String FETCHING = "fetching ";
    public static final String HIBERNATE_CFG = "hibernate.cfg.xml";
    public static final String HOLDER = "holder ";
    public static final String ID = "id";
    public static final String Id = "id";
    public static final String INVALID_CHOICE = "Enter valid choice";
    public static final String INVALID_DATA = "Invalid data entered!!";
    public static final String INVALID_INPUT = "Input given is invalid";
    public static final String IS_DUAL = "isHavingTwoAddresses";
    public static final String IS_ACTIVE = "isActive";
    public static final String LOGGER_MSG = (new StringBuilder("Error ")
                                          .append("occured while ").toString());
    public static final String MAIL_ID = "email";
    public static final String NAME = "name";
    public static final String NEW_LINE = "\n";
    public static final String NO_RECORD = "No Records found";
    public static final String PROJECT = "project";
    public static final String PROJECTS = "projects";
    public static final String PROJECT_ID = "projectId";
    public static final String RETRIVED = " retrived ";
    public static final String STATE = "state";
    public static final String STREET = "street";
    public static final String TEMP_DOOR = "temp_doorNumber";
    public static final String TEMP_STREET = "temp_street";
    public static final String TEMP_TOWN = "temp_town";
    public static final String TEMP_DISTRICT = "temp_district";
    public static final String TEMP_STATE = "temp_state";
    public static final String TITLE = "title";
    public static final String TOWN = "town";
    public static final String UPDATED = " updated ";
    public static final String UPDATING = "updating ";
    public static final String YES = "yes";
    public static final String ACTION_GET_PROJECT = "getProject";
    public static final String ACTION_ASSIGN_PROJECT = "assignProject";
    public static final String ACTION_REMOVE_PROJECT = "removeProject";
    
}
