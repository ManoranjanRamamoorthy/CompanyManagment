package com.ideas2it.employee.dao.impl;

import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.hibernate.criterion.DetachedCriteria;

import com.ideas2it.common.Constants;
import com.ideas2it.employee.dao.EmployeeDao;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * The class EmployeeDaoImpl implements the interface EmployeeDao performs CRUD
 * operations of Employee using the Employees table in the database
 * </p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public class EmployeeDaoImpl implements EmployeeDao {
    private static final AppLogger logger =
                               new AppLogger(EmployeeDaoImpl.class.getName());

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @see     com.ideas2it.employee.dao.EmployeeDao
     * @method  insertEmployee
     */
    public boolean insertEmployee(Employee employee)
                                                   throws ApplicationException {
        boolean isSuccessful = Boolean.FALSE;
        try {
            hibernateTemplate.save(employee);
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.EMPLOYEE)
                                        .append(Constants.ADDED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.ADDING)
                     .append(Constants.EMPLOYEE).append(employee.getName())
                     .toString());
        } finally {
            return isSuccessful;
        }
    }

    /**
     * @see     com.ideas2it.employee.dao.EmployeeDao
     * @method  deleteEmployee
     */
    public boolean deleteEmployee(Employee employee) throws
                                                         ApplicationException {
        return this.updateEmployee(employee);
    }

    /**
     * @see     com.ideas2it.employee.dao.EmployeeDao
     * @method  updateEmployee
     */
    public boolean updateEmployee(Employee employee)
                                                   throws ApplicationException {
        boolean isSuccessful = Boolean.FALSE;
        try {
            hibernateTemplate.update(employee);
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.EMPLOYEE)
                                        .append(Constants.UPDATED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.UPDATING)
                     .append(Constants.EMPLOYEE).append(employee.getId())
                     .toString());
        } finally {
            return isSuccessful;
        }
    }

    /**
     * @see     com.ideas2it.employee.dao.EmployeeDao
     * @method  getEmployees
     */
    public List<Employee> getEmployees() throws ApplicationException {
       List<Employee> employees = null;
        try {
            DetachedCriteria criteria =
                                     DetachedCriteria.forClass(Employee.class);
            criteria.add(Restrictions.eq(Constants.IS_ACTIVE, Boolean.TRUE));
            employees =
                    (List<Employee>) hibernateTemplate.findByCriteria(criteria);
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING)
                     .append(Constants.EMPLOYEE).toString());
        } finally {
            return employees;
        }
     }

    /**
     * @see     com.ideas2it.employee.dao.EmployeeDao
     * @method  retriveEmployeeById
     */
    public Employee retriveEmployeeById(int id) throws ApplicationException {
       Employee employee = null;
       List<Employee> employees = null;
       try {
            DetachedCriteria criteria =
                                     DetachedCriteria.forClass(Employee.class);
            criteria.add(Restrictions.eq(Constants.IS_ACTIVE, Boolean.TRUE))
                                 .add(Restrictions.eq(Constants.Id, id));
            employees =
                    (List<Employee>) hibernateTemplate.findByCriteria(criteria);
            employee = employees.get(0);
            logger.logInfo(new StringBuilder("").append(Constants.CLIENT)
                                       .append(Constants.RETRIVED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING)
                     .append(Constants.EMPLOYEE).toString());
        } finally {
            return employee;
        }
    }
}
