package com.ideas2it.employee.service;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;

/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of employee details
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */
public interface EmployeeService {

    /**
     * <p>
     * Gets the arguments and creates an object employee that
     * is assigned to an array-list employees
     * </p>
     * @param    employee
     *           the Employee object that is to be created
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean createEmployee(Employee employee) throws ApplicationException;

    /**
     * <p>
     * Performs the deletion of an employee
     * </p> 
     *
     * @param   employeeId
     *          is the Employee Id of the employee to be deleted
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean removeEmployee(int employeeId) throws ApplicationException;

    /**
     * <p>
     * Updates name of employee using employee id as reference
     * </p>
     *
     * @param   employee
     *          the employee object that is to be updated.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean modifyEmployee(Employee employee) throws ApplicationException;

    /**
     * <P>
     * Returns the employee with the given Id
     * </p>
     *
     * @param   employeeId
     *          the id of the employee
     * @returns employee
     *          the employee with the specified id
     */
    Employee retriveEmployeeById(int employeeId) throws ApplicationException;

    /**
     * <p>
     * Gives the maintained employee list
     * </p>
     *
     * @returns   employees
     *            the list of employees currently present
     */
    List<Employee> retriveEmployees() throws ApplicationException;

    /**
     * <p>
     * Assigns the given employee to the given project using their Id's
     * </p>
     * @param   employeeId
     *          the Id of the Employee who is to be assigned
     * @param   project
     *          the object of the project to which the employee is assigned
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean assignProjectToEmployee(int employeeId, Project project)
            throws ApplicationException;

    /**
     * <p>
     * Removes an employee from a particular project
     * </p>
     *
     * @param   id
     *          the Id of the Employee concerned
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean removeEmployeeFromProject(int id) throws ApplicationException;
    public List<Employee> retriveUnassignedEmployees() throws ApplicationException;
}
