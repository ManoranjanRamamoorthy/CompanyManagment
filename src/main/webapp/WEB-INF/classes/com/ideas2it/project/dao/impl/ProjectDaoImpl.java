package com.ideas2it.project.dao.impl;

import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;
import com.ideas2it.project.dao.ProjectDao;
import com.ideas2it.project.model.Project;
import com.ideas2it.util.SessionUtil;

/**
 * <p>
 * The class ProjectDaoImpl implements the interface ProjectDao and performs
 * similar actions.
 * </p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public class ProjectDaoImpl implements ProjectDao {
    private static final AppLogger logger =
                               new AppLogger(ProjectDaoImpl.class.getName());

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @see     com.ideas2it.project.dao.ProjectDao
     * @method  insertProject
     */
    public boolean insertProject(Project project) throws ApplicationException {
        boolean isSuccessful = Boolean.FALSE;
        try {
            hibernateTemplate.save(project);
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.PROJECT)
                                        .append(Constants.ADDED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.ADDING)
                     .append(Constants.PROJECT).append(project.getTitle())
                     .toString());
        } finally {
            return isSuccessful;
        }
    }

    /**
     * @see     com.ideas2it.project.dao.ProjectDao
     * @method  deleteProject
     */
    public boolean deleteProject(Project project) throws ApplicationException {
        return this.updateProject(project);
    }

    /**
     * @see     com.ideas2it.project.dao.ProjectDao
     * @method  updateProject
     */
    public boolean updateProject(Project project) throws ApplicationException {
        boolean isSuccessful = Boolean.FALSE;
        try {
            hibernateTemplate.update(project);
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.PROJECT)
                                        .append(Constants.UPDATED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.UPDATING)
                     .append(Constants.PROJECT).append(project.getId())
                     .toString());
        } finally {
            return isSuccessful;
        }
    }

    /**
     * @see     com.ideas2it.project.dao.ProjectDao
     * @method  getProjects
     */
    public List<Project> getProjects() throws ApplicationException {
        List<Project> projects = null;
        try {
            DetachedCriteria criteria =
                                     DetachedCriteria.forClass(Project.class);
            criteria.add(Restrictions.eq(Constants.IS_ACTIVE, Boolean.TRUE));
            projects =
                    (List<Project>) hibernateTemplate.findByCriteria(criteria);
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING)
                     .append(Constants.PROJECT).toString());
        } finally {
            return projects;
        }
     }

    /**
     * @see     com.ideas2it.project.dao.ProjectDao
     * @method  retriveProjectById
     */
    public Project retriveProjectById(int id) throws ApplicationException {
        List<Project> projects = null;
        Project project = null;
        try {
            DetachedCriteria criteria =
                                     DetachedCriteria.forClass(Project.class);
            criteria.add(Restrictions.eq(Constants.IS_ACTIVE, Boolean.TRUE))
                          .add(Restrictions.eq(Constants.Id, id));
            projects =
                     (List<Project>) hibernateTemplate.findByCriteria(criteria);
            project = projects.get(0);
            logger.logInfo(new StringBuilder("").append(Constants.PROJECT)
                                       .append(Constants.RETRIVED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING)
                     .append(Constants.PROJECT)
                     .toString());
        } finally {
            return project;
        }
    }
}
