package com.ideas2it.project.model;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
    
/*
 * <p>
 * The class Project has getters and setters that help 
 * to access the class members 
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */

public class Project {
    private boolean isActive = Boolean.TRUE;
    private int id;
    private String description;
    private String domain;
    private String title;
    private Client client;
    private List<Employee> employees;

    public Project() {
    }

    public Project(String title, String description,
                                       String domain ) {
        this.setDescription(description);
        this.setDomain(domain);
        this.setTitle(title);
        this.setClient(null);
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getTitle() {
        return this.title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return this.employees;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return this.client;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    /**
     * <p>
     * Displays the details of the project
     * </p>
     *
     * @overrides toString() method
     *
     * @returns the whole project object as a string
     */
    public String toString() {
        return (new StringBuilder(Constants.ID).append(this.id)
        .append(Constants.NEW_LINE)
        .append(Constants.TITLE).append(this.title)
        .append(Constants.NEW_LINE).append(Constants.DOMAIN)
        .append(this.domain).append(Constants.NEW_LINE)
        .append(Constants.DESCRIPTION)
        .append(this.description)
        .append(Constants.NEW_LINE)
        .append(Constants.EMPLOYEES)
        .append(this.employees).toString());
        
    }
}
