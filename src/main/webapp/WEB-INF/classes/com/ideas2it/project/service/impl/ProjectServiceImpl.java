package com.ideas2it.project.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.dao.impl.ProjectDaoImpl;
import com.ideas2it.project.dao.ProjectDao;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.ProjectService;

/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of project details
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */
public class ProjectServiceImpl implements ProjectService {
    private EmployeeService employeeService;
    private ProjectDao projectDao;

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public void setProjectDao(ProjectDao projectDao) {
        this.projectDao = projectDao;
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  createProject
     */
    public Project createProject(Project project) throws ApplicationException {
        return project;
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  removeProject
     */
    public boolean removeProject(int id) throws ApplicationException {
        Project project = retriveProjectById(id);
        if (null != project) {
            project.setIsActive(Boolean.FALSE);
            project.setClient(null);
            return projectDao.deleteProject(project);
        }
        return Boolean.FALSE;
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  modifyProjectDetails
     */
    public boolean modifyProject(Project project) throws ApplicationException {
        return projectDao.updateProject(project);
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  retriveProjects
     */
    public List<Project> retriveProjects() throws ApplicationException {
        return projectDao.getProjects();
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  retriveProjectById
     */
    public Project retriveProjectById(int projectId)
                                                   throws ApplicationException {
        return projectDao.retriveProjectById(projectId);
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  assignEmployeesToProject
     */
    public boolean assignEmployeesToProject(int projectId,
                        List<Integer> employeeIds) throws ApplicationException {
        List<Employee> employees = new ArrayList<>();

        Project project = retriveProjectById(projectId);
        if (null != project) {
            for (int employeeId : employeeIds) {
                Employee employee =
                                employeeService.retriveEmployeeById(employeeId);
                project.getEmployees().add(employee);
            }
         return projectDao.updateProject(project);
         }
         return Boolean.FALSE;
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  deassignEmployeeFromProject
     */
    public boolean deassignEmployeeFromProject(int projectId, int employeeId)
                                                   throws ApplicationException {
        Project project = retriveProjectById(projectId);
        Employee employee = employeeService.retriveEmployeeById(employeeId);
        if ((null != employee) && (null != project)) {
            project.getEmployees().remove(employee);
            projectDao.updateProject(project);
            return employeeService.removeEmployeeFromProject(employeeId);
        }
        return Boolean.FALSE;            
    }

    /**
     * @see     com.ideas2it.project.service.ProjectService
     * @method  assignClientToProject
     */
    public boolean assignClientToProject(int projectId, Client client)
                                                   throws ApplicationException {
        Project project = retriveProjectById(projectId);
        if (null != project) {
            project.setClient(client);
            return projectDao.updateProject(project);
        }
        return Boolean.FALSE;
    }

    /**
     * @see     com.ideas2it.employee.service.EmployeeService
     * @method  removeEmployeeFromProject
     */
    public boolean removeProjectFromClient(int projectId)
                                                   throws ApplicationException {
        Project project = this.retriveProjectById(projectId);
        if (null != project) {
            project.setIsActive(Boolean.FALSE);
            return projectDao.updateProject(project);
        }
        return Boolean.FALSE;
    }
}
