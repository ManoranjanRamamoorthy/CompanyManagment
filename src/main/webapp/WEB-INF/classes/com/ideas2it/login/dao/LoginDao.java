package com.ideas2it.login.dao;

import java.util.List;

import com.ideas2it.exception.ApplicationException;
import com.ideas2it.login.model.Login;

/**
 * <p>
 * The interface LoginDao performs storing, updating, deleting and retrieving
 * data in the form of tables in the database.
 * </p>
 * @created    01/11/2017
 * @author     Manoranjan.R
 */
public interface LoginDao {

    /**
     * <p>
     * Inserts the object login into the login table.
     * </p>
     *
     * @param   login
     *          the login object that is to be inserted to the table.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean insertLogin(Login login) throws ApplicationException;

    /**
     * <p>
     * Deletes a login from the table.
     * </p>
     *
     * @param   login
     *          the login object that is to be deleted
     *     
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean deleteLogin(Login login) throws ApplicationException;

    /**
     * <p>
     * Updates the details of a login.
     * </p>
     *
     * @param   login
     *          the login object that is to be updated.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean updateLogin(Login login) throws ApplicationException;

    /**
     * <p>
     * Returns all the logins currently present as a list.
     * </p>
     * 
     * @returns  logins
     *           the list containing all existing logins.
     */
    List<Login> getLogins() throws ApplicationException;

}
