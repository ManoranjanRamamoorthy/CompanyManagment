package com.ideas2it.login.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.employee.service.EmployeeService;
import com.ideas2it.employee.service.impl.EmployeeServiceImpl;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.client.service.impl.ClientServiceImpl;
import com.ideas2it.login.model.Login;
import com.ideas2it.login.service.LoginService;
import com.ideas2it.login.dao.LoginDao;
import com.ideas2it.login.dao.impl.LoginDaoImpl;

/**
 * <p>
 * </p>
 *
 * @created    01/11/2017
 * @author     Manoranjan.R
 */
public class LoginServiceImpl implements LoginService {
    private EmployeeService employeeService;
    private LoginDao loginDao;
    private ClientService clientService;

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public void setLoginDao(LoginDao loginDao) {
        this.loginDao = loginDao;
    }

    /**
     * @see     com.ideas2it.login.service.LoginService
     * @method  createLogin
     */
    public Boolean createLogin(Login login) throws ApplicationException {
        return loginDao.insertLogin(login);
    }

    /**
     * @see     com.ideas2it.login.service.LoginService
     * @method  removeLogin
     */
    public boolean removeLogin(Login login) throws ApplicationException {
        login.setIsActive(Boolean.FALSE);
        return this.modifyLogin(login);
    }

    /**
     * @see     com.ideas2it.login.service.LoginService
     * @method  modifyLoginDetails
     */
    public boolean modifyLogin(Login login) throws ApplicationException {
        return loginDao.updateLogin(login);
    }

    /**
     * @see     com.ideas2it.login.service.LoginService
     * @method  retriveLogins
     */
    public List<Login> retriveLogins() throws ApplicationException {
        return loginDao.getLogins();
    }
}
