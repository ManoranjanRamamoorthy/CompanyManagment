package com.ideas2it.login.dao.impl;

import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;
import com.ideas2it.login.dao.LoginDao;
import com.ideas2it.login.model.Login;

/**
 * <p>
 * The class LoginDaoImpl implements the interface LoginDao and performs
 * similar actions.
 * </p>
 * @created    01/11/2017
 * @author     Manoranjan.R
 */
public class LoginDaoImpl implements LoginDao {
    private static final AppLogger logger =
                               new AppLogger(LoginDaoImpl.class.getName());

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @see     com.ideas2it.login.dao.LoginDao
     * @method  insertLogin
     */
    public boolean insertLogin(Login login) throws ApplicationException {
        boolean isSuccessful = Boolean.FALSE;
        try {
            hibernateTemplate.save(login);
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.PROJECT)
                                        .append(Constants.ADDED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.ADDING)
                     .append(Constants.PROJECT)
                     .toString());
        } finally {
            return isSuccessful;
        }
    }

    /**
     * @see     com.ideas2it.login.dao.LoginDao
     * @method  deleteLogin
     */
    public boolean deleteLogin(Login login) throws ApplicationException {
        return this.updateLogin(login);
    }

    /**
     * @see     com.ideas2it.login.dao.LoginDao
     * @method  updateLogin
     */
    public boolean updateLogin(Login login) throws ApplicationException {
        boolean isSuccessful = Boolean.FALSE;
        try {
            hibernateTemplate.update(login);
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.PROJECT)
                                        .append(Constants.UPDATED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.UPDATING)
                     .append(Constants.PROJECT).append(login.getId())
                     .toString());
        } finally {
            return isSuccessful;
        }
    }

    /**
     * @see     com.ideas2it.login.dao.LoginDao
     * @method  getLogins
     */
    public List<Login> getLogins() throws ApplicationException {
        List<Login> logins = null;
        try {
            DetachedCriteria criteria =
                                     DetachedCriteria.forClass(Login.class);
            logins =
                    (List<Login>) hibernateTemplate.findByCriteria(criteria);
        } catch (HibernateException exception) {
            logger.logError(exception);
            exception.printStackTrace();
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING)
                     .append("login").toString());
        } finally {
            return logins;
        }
     }
}
