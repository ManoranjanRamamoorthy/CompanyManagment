package com.ideas2it.login.model;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
    
/*
 * <p>
 * The Login is the model class that acts as login object for the Clients and
 * Employees
 *
 * @created    01/11/2017
 * @author     Manoranjan.R
 */

public class Login {
    private boolean isActive = Boolean.TRUE;
    private boolean isEmployee = Boolean.FALSE;
    private int id;
    private String username;
    private String password;
    private Client client;
    private Employee employee;

    public Login() {
    }

    public Login(String username, String password, Client client) {
        this.setUsername(username);
        this.setPassword(password);
        this.setClient(client);
    }

    public Login(String username, String password, Employee employee) {
        this.setUsername(username);
        this.setPassword(password);
        this.setEmployee(employee);
    }

    public Login(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getUsername() {
        return this.username;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Client getClient() {
        return this.client;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public void setIsEmployee(boolean isEmployee) {
        this.isEmployee = isEmployee;
    }

    public boolean getIsEmployee() {
        return this.isEmployee;
    }
}
