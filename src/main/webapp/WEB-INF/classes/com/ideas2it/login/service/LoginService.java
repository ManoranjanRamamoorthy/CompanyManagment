package com.ideas2it.login.service;

import java.util.List;

import com.ideas2it.login.model.Login;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of project details
 * </p>
 *
 * @created    01/11/2017
 * @author     Manoranjan.R
 */
public interface LoginService {

    /**
     * <p>
     * </p>
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    Boolean createLogin(Login login) throws ApplicationException;

    /**
     * <p>
     * </p> 
     *
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean removeLogin(Login login) throws ApplicationException;

    /**
     * <p>
     * </p>
     *
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean modifyLogin(Login login) throws ApplicationException;

    /**
     * <p>
     * </p>
     *
     */
    List<Login> retriveLogins() throws ApplicationException;
}
