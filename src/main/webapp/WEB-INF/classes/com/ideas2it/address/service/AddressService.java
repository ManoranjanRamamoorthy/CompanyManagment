package com.ideas2it.address.service;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.client.model.Client;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of Address details
 * </p>
 *
 * @created    12/09/2017
 * @author     Manoranjan.R
 */
public interface AddressService {

    /**
     * <p>
     * Gets the arguements and creates an object address
     * </p>
     *
     * @param   doorNumber
     *          the door number of the address
     * @param   street
     *          the name of the street
     * @param   town
     *          the name of the town
     * @param   district
     *          the name of the district
     * @param   state
     *          the name of the state
     *
     * @returns address
     *          the Address object created using the given values.
     * 
     */
     Address createAddress(String doorNumber, String street,
        String district, String town, String state) throws ApplicationException;

    /**
     * <p>
     * Performs the deletion of an address
     * </p> 
     *
     * @param   address
     *          the address object that is to be deleted.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean deleteAddress(Address address) throws ApplicationException;
        
    /**
     * <p>
     * Updates the address of a particular holder using the parameters
     * as reference.
     * </p>
     *
     * @param   address
     *          the address object that is to be updated.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean updateAddress(Address address) throws ApplicationException;

    /**
     * <P>
     * Returns the address list in which all addresses have been stored
     * </p>
     *
     * @returns allAddresses
     *          the list containing all addresses present
     */
    List<Address> getAllAddresses() throws ApplicationException;
}
