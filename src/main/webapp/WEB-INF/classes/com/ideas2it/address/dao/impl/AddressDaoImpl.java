package com.ideas2it.address.dao.impl;

import java.lang.StringBuilder;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ideas2it.address.dao.AddressDao;
import com.ideas2it.address.model.Address;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;
import com.ideas2it.util.SessionUtil;

/**
 * <p>
 * Performs CRUD operations of address objects.
 *</p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public class AddressDaoImpl implements AddressDao {
    private static final AppLogger logger =
                               new AppLogger(AddressDaoImpl.class.getName());

    /**
     * @see     com.ideas2it.address.dao.AddressDao
     * @method  insertAddress
     */
    public void insertAddress(Address address) throws ApplicationException {
        Session session = SessionUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(address);
            transaction.commit();
            logger.logInfo(new StringBuilder("").append(Constants.ADDRESS)
                                        .append(Constants.ADDED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            if(null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.ADDING)
                     .append(Constants.ADDRESS)
                     .toString());
        } finally {
            session.close();
        }
    }

    /**
     * @see     com.ideas2it.address.dao.AddressDao
     * @method  insertAddress
     */
    public boolean deleteAddress(Address address) throws ApplicationException {
        return this.updateAddress(address);
    }

    /**
     * @see     com.ideas2it.address.dao.AddressDao
     * @method  insertAddress
     */
    public List<Address> getAddresses() throws ApplicationException {
        Session session = SessionUtil.getSession();
        List<Address> addresses = null;
        try {
            Criteria criteria = session.createCriteria(Address.class)
                     .add(Restrictions.eq(Constants.IS_ACTIVE, Boolean.TRUE));
            addresses = criteria.list();
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING).append(Constants.ADDRESSES)
                     .toString());
        } finally {
            session.close();
            return addresses;
        }
    }

    /**
     * @see     com.ideas2it.address.dao.AddressDao
     * @method  insertAddress
     */
    public boolean updateAddress(Address address) throws ApplicationException {
        Session session = SessionUtil.getSession();
        Transaction transaction = null;
        boolean isSuccessful = Boolean.FALSE;
        try {
            transaction = session.beginTransaction();
            session.update(address);
            transaction.commit();
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.ADDRESS)
                                        .append(Constants.UPDATED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            if(null != transaction) {
                transaction.rollback();
            }
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.UPDATING).append(Constants.ADDRESS)
                     .toString());
        } finally {
            session.close();
            return isSuccessful;
        }
    }
}
