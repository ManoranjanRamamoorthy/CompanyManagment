package com.ideas2it.address.model;

import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;

/**
 * <p>
 * The class Address has getters and setters that help 
 * to access the class members 
 * </p>
 *
 * @created    13/09/2017
 * @author     Manoranjan.R
 */
public class Address {
    private boolean isActive = Boolean.TRUE;
    private int id;
    private String district;
    private String doorNumber;
    private String state;
    private String street;
    private String town;
    private Client client;
    private Employee employee;

    public Address() {
    }

    public Address(String doorNumber, String street, String town,
                                 String district, String state) {
        this.setDistrict(district);
        this.setDoorNumber(doorNumber);
        this.setState(state);
        this.setStreet(street);
        this.setTown(town);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setDoorNumber(String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getDoorNumber() {
        return this.doorNumber;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return this.street;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTown() {
        return this.town;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    /**
     * <p>
     * Displays the address details stored
     * </p>
     *
     * @overrides toString to print Address object
     *
     * @returns   the address object in the form of a string.
     */
    public String toString() {
        return (new StringBuilder(Constants.NEW_LINE)
               .append(Constants.DOOR_NO).append(getDoorNumber())
               .append(Constants.NEW_LINE)
               .append(Constants.STREET).append(getStreet())
               .append(Constants.NEW_LINE)
               .append(Constants.TOWN).append(getTown())
               .append(Constants.NEW_LINE)
               .append(Constants.DISTRICT).append(getDistrict())
               .append(Constants.NEW_LINE)
               .append(Constants.STATE).append(getState())
               .append(Constants.NEW_LINE).toString());
               
    }
}
