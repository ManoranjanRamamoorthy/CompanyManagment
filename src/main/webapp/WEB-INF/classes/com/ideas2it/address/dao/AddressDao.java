package com.ideas2it.address.dao;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Performs CRUD operations of address objects using the table from the database
 *</p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public interface AddressDao {

    /**
     * <p>
     * Inserts an address to the table in the database.
     * </p>
     *
     * @param   address
     *          the address object which is to be added to the table.
     */
    void insertAddress(Address address) throws ApplicationException;

    /**
     * <p>
     * Deletes an address in from the table using id as a reference.
     * </p>
     *
     * @param   address
     *          the address object that is to be deleted
     */
    boolean deleteAddress(Address address) throws ApplicationException;

    /**
     * <p>
     * Updates the address with the new details entered.
     * </p>
     *
     * @param   address
     *          the address object to be updated.
     */
    boolean updateAddress(Address address) throws ApplicationException;

    /**
     * <p>
     * Selects all the addresses available.
     * </p>
     *
     * @returns    addresses
     *             the address list containing all the addresses.
     */
    List<Address> getAddresses() throws ApplicationException;
}


