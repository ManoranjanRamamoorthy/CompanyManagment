package com.ideas2it.client.dao.impl;

import java.lang.StringBuilder;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.client.dao.ClientDao;
import com.ideas2it.client.model.Client;
import com.ideas2it.common.Constants;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.logger.AppLogger;

/**
 * <p>
 * The class ClientDaoImpl implements the interface ClientDao performs CRUD
 * operations of the client using the client table in the database
 * </p> 
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public class ClientDaoImpl implements ClientDao {
    private static final AppLogger logger =
                               new AppLogger(ClientDaoImpl.class.getName());

    private HibernateTemplate hibernateTemplate;

    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    /**
     * @see     com.ideas2it.client.dao.ClientDao
     * @method  insertClient
     */
    public boolean insertClient(Client client) throws ApplicationException {
        boolean isSuccessfull = Boolean.FALSE;
        try {
            hibernateTemplate.save(client);
            isSuccessfull = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.CLIENT)
                                        .append(Constants.ADDED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.ADDING)
                     .append(Constants.CLIENT).append(client.getName())
                     .toString());
        } finally {
            return isSuccessfull;
        }
    }

    /**
     * @see     com.ideas2it.client.dao.ClientDao
     * @method  deleteClient
     */
    public boolean deleteClient(Client client) throws ApplicationException {
        return this.updateClient(client);
    }

    /**
     * @see     com.ideas2it.client.dao.ClientDao
     * @method  updateClient
     */
    public boolean updateClient(Client client) throws ApplicationException {
        boolean isSuccessful = Boolean.FALSE;
        try {
            hibernateTemplate.update(client);
            isSuccessful = Boolean.TRUE;
            logger.logInfo(new StringBuilder("").append(Constants.CLIENT)
                                        .append(Constants.UPDATED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.UPDATING)
                     .append(Constants.CLIENT).append(client.getId())
                     .toString());
        } finally {
            return isSuccessful;
        }
    }

    /**
     * @see     com.ideas2it.client.dao.ClientDao
     * @method  getClients
     */
    public List<Client> getClients() throws ApplicationException {
        List<Client> clients = null;
        try {
            DetachedCriteria criteria =
                                     DetachedCriteria.forClass(Client.class);
            criteria.add(Restrictions.eq(Constants.IS_ACTIVE, Boolean.TRUE));
            clients = (List<Client>) hibernateTemplate.findByCriteria(criteria);
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING)
                     .append(Constants.CLIENT).toString());
        } finally {
            return clients;
        }
     }
     
    /**
     * @see     com.ideas2it.client.dao.ClientDao
     * @method  retriveClientById
     */  
     public Client retriveClientById(int id) throws ApplicationException {
        List<Client> clients = null;
        Client client = null;
        try {
            DetachedCriteria criteria =
                                     DetachedCriteria.forClass(Client.class);
            criteria.add(Restrictions.eq(Constants.IS_ACTIVE, Boolean.TRUE))
                          .add(Restrictions.eq(Constants.Id, id));
            clients = (List<Client>) hibernateTemplate.findByCriteria(criteria);
            client = clients.get(0);
            logger.logInfo(new StringBuilder("").append(Constants.CLIENT)
                                       .append(Constants.RETRIVED).toString());
        } catch (HibernateException exception) {
            logger.logError(exception);
            throw new ApplicationException(new
                     StringBuilder(Constants.LOGGER_MSG)
                     .append(Constants.FETCHING)
                     .append(Constants.CLIENT).toString());
        } finally {
            return client;
        }
    }
}
