package com.ideas2it.client.dao;

import java.util.List;

import com.ideas2it.client.model.Client;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * The interface ClientDao performs CRUD operations of the client using the 
 * client table in the database
 * </p>
 * @created    28/09/2017
 * @author     Manoranjan.R
 */
public interface ClientDao {

    /**
     * <p>
     * Inserts the object Client into the client table
     * </p>
     *
     * @param   client
     *          the client object that is to be inserted to the table
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean insertClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Deletes a client from the table
     * </p>
     *
     * @param   client
     *          the client object that is to be deleted
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean deleteClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Updates the details of a client using client id as reference
     * </p>
     *
     * @param   client
     *          the client object that is to be updated
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean updateClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Returns all the clients currently present as a list
     * </p>
     * 
     * @returns  clients
     *           the list containing all existing clients
     */
    List<Client> getClients() throws ApplicationException;
     
     /**
      * <p>
      * Retrieves the client object that corresponds to the given Id.
      * </p>
      *
      * @param    id
      *           the id of the client who is to be pulled out.
      * @returns  client
      *           the client object that corresponds to the given id.
      */  
     Client retriveClientById(int id) throws ApplicationException;
}
