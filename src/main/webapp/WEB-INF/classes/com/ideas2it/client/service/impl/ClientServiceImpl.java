package com.ideas2it.client.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.client.dao.ClientDao;
import com.ideas2it.client.dao.impl.ClientDaoImpl;
import com.ideas2it.client.model.Client;
import com.ideas2it.client.service.ClientService;
import com.ideas2it.common.Constants;
import com.ideas2it.employee.model.Employee;
import com.ideas2it.exception.ApplicationException;
import com.ideas2it.project.model.Project;
import com.ideas2it.project.service.impl.ProjectServiceImpl;
import com.ideas2it.project.service.ProjectService;

/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of client details
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */
public class ClientServiceImpl implements ClientService {
    private ClientDao clientDao;
    private ProjectService projectService;

    public void setClientDao(ClientDao clientDao) {
        this.clientDao = clientDao;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * @see      com.ideas2it.client.service.ClientService
     * @method   createClient
     */
    public boolean createClient(Client client) throws ApplicationException {
        return clientDao.insertClient(client);
    }

    /**
     * @see      com.ideas2it.client.service.ClientService
     * @method   removeClient
     */
    public boolean removeClient(int clientId) throws ApplicationException {
        Client client = clientDao.retriveClientById(clientId);
        if (null != client) {
            client.setIsActive(Boolean.FALSE);
            for (Project project : client.getProjects() ) {
                project.setIsActive(Boolean.FALSE);
                for (Employee employee : project.getEmployees()) {
                    employee.setIsActive(Boolean.FALSE);
                }
            }
            for (Address address : client.getAddresses()) {
                address.setIsActive(Boolean.FALSE);
            }
            clientDao.deleteClient(client);
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * @see      com.ideas2it.client.service.ClientService
     * @method   modifyClient
     */
    public boolean modifyClient(Client client) throws ApplicationException {
        return clientDao.updateClient(client);
    }

    /**
     * @see      com.ideas2it.client.service.ClientService
     * @method   retriveClients
     */
    public List<Client> retriveClients() throws ApplicationException {
        return clientDao.getClients();
    }

    /**
     * @see      com.ideas2it.client.service.ClientService
     * @method   retriveClientById
     */
    public Client retriveClientById(int clientId)
                                                   throws ApplicationException {
        return clientDao.retriveClientById(clientId);
    }

    /**
     * @see     co.ideas2it.client.service.ClientService
     * @method  addProjectsToClient
     */
    public boolean addProjectsToClient(int clientId,
        List<Integer> projectIds) throws ApplicationException {
        List<Project> projects = new ArrayList<>();
        Client client = retriveClientById(clientId);
        if (null != client) {
            for (int projectId : projectIds) {
                Project project = projectService.retriveProjectById(projectId);
                projects.add(project);
                client.getProjects().add(project);
                projectService.assignClientToProject(projectId, client);
            }
            return clientDao.updateClient(client);
        }
        return Boolean.FALSE;
    }

    /**
     * @see     co.ideas2it.client.service.ClientService
     * @method  removeProjectFromClient
     */
    public boolean removeProjectFromClient(int clientId, int projectId)
            throws ApplicationException {
        Client client = this.retriveClientById(clientId);
        Project project = projectService.retriveProjectById(projectId);
        if ((null != client) && (null != project)) {
            client.getProjects().remove(project);
            clientDao.updateClient(client);
        }
        return projectService.removeProject(projectId);
    }
}
