package com.ideas2it.client.model;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.project.model.Project;
import com.ideas2it.login.model.Login;

/**
 * <p>
 * The class Client has getters and setters that help 
 * to access the class members 
 * </p>
 *
 * @created    12/09/2017
 * @author     Manoranjan.R
 */

public class Client {
    private boolean isActive = Boolean.TRUE;
    private int id;
    private String email;
    private String name;
    private List<Address> addresses;
    private List<Project> projects;
    private Login login;

    public Client() {
    }

    public Client(String name, String email, List<Address> addresses ) {
        this.setAddresses(addresses);
        this.setEmail(email);
        this.setName(name);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Address> getAddresses() {
        return this.addresses;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Project> getProjects() {
        return this.projects;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Login getLogin() {
        return this.login;
    }

}
