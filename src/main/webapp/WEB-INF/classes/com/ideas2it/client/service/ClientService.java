package com.ideas2it.client.service;

import java.util.List;

import com.ideas2it.address.model.Address;
import com.ideas2it.address.service.AddressService;
import com.ideas2it.client.model.Client;
import com.ideas2it.exception.ApplicationException;

/**
 * <p>
 * Performs specific functions regarding the 
 * addition, updation, and deletion of client details
 * </p>
 *
 * @created    07/09/2017
 * @author     Manoranjan.R
 */
public interface ClientService {

    /**
     * <p>
     * Gets the arguments and creates an object client that
     * is assigned to an array-list clients
     * </p>
     *
     * @param   client
     *          the client object that is to be added.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean createClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Performs the deletion of an client
     * </p> 
     *
     * @param   clientId
     *          is the Client Id of the client to be deleted.
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean removeClient(int clientId) throws ApplicationException;

    /**
     * <p>
     * Updates name of client using client id as reference
     * </p>
     *
     * @param   client
     *          the client object that is to be updated.
     *
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean modifyClient(Client client) throws ApplicationException;

    /**
     * <p>
     * Getter for getting the client list
     * </p>
     *
     * @returns clients
     *          the list containing all clients
     */
    List<Client> retriveClients() throws ApplicationException;

    /**
     * <P>
     * Returns the client with the given Id
     * </p>
     *
     * @param   clientId
     *          the id of the client
     * @returns client
     *          the client with the specified id
     */
    Client retriveClientById(int clientId) throws ApplicationException;

    /**
     * <p>
     * Assigns the projects to a particular client
     * </p>
     *
     * @param   clientId
     *          the Id of the client to which the Projects are to be assigned
     * @param   projectIds
     *          the Id's of the projects that are given by the client
     *
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean addProjectsToClient(int clientId, List<Integer> projectIds)
            throws ApplicationException;

    /**
     * <p>
     * Removes an project from the corresponding client
     * </p>
     *
     * @param   clientId
     *          the Id of the client by whom projects are assigned
     * @param   projectId
     *          the Id of the project which is to be removed
     *
     *
     * @returns TRUE if successful \ FALSE if unsuccessful.
     */
    boolean removeProjectFromClient(int clientId, int projectId)
            throws ApplicationException;
}
