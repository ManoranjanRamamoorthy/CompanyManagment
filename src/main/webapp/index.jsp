<!DOCTYPE html>
<html>
<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:400,300,500);
*:focus {
  outline: none;
}

body {
  margin: 0;
  padding: 0;
  background: #DDD;
  font-size: 16px;
  color: #222;
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
}

#login-box {
  position: relative;
  margin: 5% auto;
  width: 600px;
  height: 500px;
  background: #FFF;
  border-radius: 2px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
}

.left {
  position: absolute;
  top: 0;
  left: 0;
  box-sizing: border-box;
  padding: 40px;
  width: 300px;
  height: 400px;
  border-radius: 2px;
}

h1 {
  margin: 0 0 20px 0;
  font-weight: 300;
  font-size: 28px;
}

input[type="text"],
input[type="password"] {
  display: block;
  box-sizing: border-box;
  margin-bottom: 20px;
  padding: 4px;
  width: 220px;
  height: 32px;
  border: none;
  border-bottom: 1px solid #AAA;
  font-family: 'Roboto', sans-serif;
  font-weight: 400;
  font-size: 15px;
  transition: 0.2s ease;
}

input[type="text"]:focus,
input[type="password"]:focus {
  border-bottom: 2px solid #16a085;
  color: #16a085;
  transition: 0.2s ease;
}

input[type="submit"] {
  margin-top: 28px;
  width: 120px;
  height: 32px;
  background: #16a085;
  border: none;
  border-radius: 2px;
  color: #FFF;
  font-family: 'Roboto', sans-serif;
  font-weight: 500;
  text-transform: uppercase;
  transition: 0.1s ease;
  cursor: pointer;
}

input[type="submit"]:hover,
input[type="submit"]:focus {
  opacity: 0.8;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
  transition: 0.1s ease;
}

input[type="submit"]:active {
  opacity: 1;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
  transition: 0.1s ease;
}

.or {
  position: absolute;
  top: 180px;
  left: 280px;
  width: 40px;
  height: 40px;
  background: #DDD;
  border-radius: 50%;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
  line-height: 40px;
  text-align: center;
}

.right {
  position: absolute;
  top: 0;
  right: 0;
  box-sizing: border-box;
  padding: 40px;
  width: 300px;
  height: 400px;
  background-size: cover;
  background-position: center;
  border-radius: 0 2px 2px 0;
  border-radius: 2px;
}

.right .loginwith {
  display: block;
  margin-bottom: 40px;
  font-size: 28px;
  color: #FFF;
  text-align: center;
}

.left-in input {
    float:left;
}

.right-in input {
    float:right;
}

.button {
    background-color: #16a085;
    border: none;
    color: white;
    padding: 9px 13px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    margin: 0px 0px;
    cursor: pointer;
    font-size: 12px;
    height: 32px;
}
}

</style>
<head>
<script type="text/javascript">

function checkForm(form)
  {
    re = /^\w+$/;
    if(!re.test(form.username.value)) {
      alert("Username must contain only letters, numbers and underscores!");
      form.username.focus();
      return false;
    }

    if(form.password_temporary.value != "temp_pwd") {
      alert("Please check the temporary password entered.");
      form.password_temporary.focus();
      return false;
    }

    if(form.password.value != "" && form.password.value == form.password_confirm.value) {
      if(form.password.value.length < 6) {
        alert("Password must contain at least six characters!");
        form.password.focus();
        return false;
      }
      if(form.password.value == form.username.value) {
        alert("Password must be different from Username!");
        form.password.focus();
        return false;
      }
      re = /[0-9]/;
      if(!re.test(form.password.value)) {
        alert("Password must contain at least one number (0-9)!");
        form.password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password.value)) {
        alert("Password must contain at least one lowercase letter (a-z)!");
        form.password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password.value)) {
        alert("Password must contain at least one uppercase letter (A-Z)!");
        form.password.focus();
        return false;
      }
    } else {
      alert("Please check that you've entered and confirmed your password!");
      form.password.focus();
      return false;
    }

    alert("Signed Up Complete");
    return true;
  }



function showPassword() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}



password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;


</script>
</head>
<body>

 <%
        String myname =  (String)session.getAttribute("user");
        
        if(myname!=null)
            {
             %><jsp:forward page="/jsp/welcome.jsp" /><%
            }
        else 
            {
            %>




<div id="login-box">
  <div class="left">
  <form action="LoginServlet" method="post"  onsubmit="return checkForm(this);">
  <input type="hidden" name="action" value="signUp">
    <h3>New User? Sign up</h3>

    Select User Type:-<br>
    <input  type="radio" name="userType" required>Employee
    <input type="radio" name="userType" required>Client    
    <br>
    <div class="left-in">
    <input style="width:20%" type="text" name="id" placeholder="Id" required/>
    </div>
    <div class="right-in">
    <input style="width:75%" type="text" name="name" placeholder="Your Name" required/>
    </div>
    <div class="left-in">
    <input style="width:65%" type="text" name="username" placeholder="Username" required/>
    </div>
    <button style="width:35%" class="button" type="button">VALIDATE</button>
    <input type="password" name="password_temporary" placeholder="Temporary Password" required/>
    <input type="password" name="password" placeholder="New password" required/>
    <input type="password" name="password_confirm" placeholder="Confirm New password" required/>
    <input type="submit" value="Sign up"  />
  </form>
  </div>
  
  <div class="right">
<form action="LoginServlet" method="post">
<input type="hidden" name="action" value="login">

  <div class="container"><br><br>
  <h2> Login </h2>${user}
    <input type="text" placeholder="Enter Username" name="username" required>

    <input type="password" placeholder="Enter Password" name="password" required>
        
    <input type="submit" value="login"/><br>
    <font color="red">${Msg}<font color="red">
  </div>
  </div>
  <div class="or">OR</div>
</div>



</form>
            <% 
            }
         
             
            %>

</body>
</html>
